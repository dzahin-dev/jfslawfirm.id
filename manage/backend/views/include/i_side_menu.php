<? 
    $current = (isset($this->current_menu)? $this->current_menu : ""); 
    $menu = array(
        array('Dashboard','dashboard','fi-bar-graph'),
        array('Homepage','homepage','fi-monitor'),
        array('About Us','about','fi-head'),
        array('News Update','news','fi-paper'),
        array('Gallery','gallery','fi-image'),
        array('Contact US','contact','fi-mail'),
        array('Global Config','config','fi-cog'),
        array('Logout','logout','fi-air-play'),
    );
?>

<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Navigation</li>
                <?foreach ($menu as $m): ?>
                    <li>
                        <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>"><i class="<?=$m[2];?>"></i><span> <?=$m[0];?> </span> </a>
                    </li>
                <?endforeach;?>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- <ul style="background: red;
padding-left: 277px;
font-size: 32px;">
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-eye"></i>
    <span>fi-eye</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-paper-clip"></i>
    <span>fi-paper-clip</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-mail"></i>
    <span>fi-mail</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-mail"></i>
    <span>fi-mail</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-toggle"></i>
    <span>fi-toggle</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-layout"></i>
    <span>fi-layout</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-link"></i>
    <span>fi-link</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-bell"></i>
    <span>fi-bell</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-lock"></i>
    <span>fi-lock</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-unlock"></i>
    <span>fi-unlock</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-ribbon"></i>
    <span>fi-ribbon</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-image"></i>
    <span>fi-image</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-signal"></i>
    <span>fi-signal</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-target"></i>
    <span>fi-target</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-clipboard"></i>
    <span>fi-clipboard</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-clock"></i>
    <span>fi-clock</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-clock"></i>
    <span>fi-clock</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-watch"></i>
    <span>fi-watch</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-air-play"></i>
    <span>fi-air-play</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-camera"></i>
    <span>fi-camera</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-video"></i>
    <span>fi-video</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-disc"></i>
    <span>fi-disc</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-printer"></i>
    <span>fi-printer</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-monitor"></i>
    <span>fi-monitor</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-server"></i>
    <span>fi-server</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-cog"></i>
    <span>fi-cog</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-heart"></i>
    <span>fi-heart</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-paragraph"></i>
    <span>fi-paragraph</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-align-justify"></i>
    <span>fi-align-justify</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-align-left"></i>
    <span>fi-align-left</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-align-center"></i>
    <span>fi-align-center</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-align-right"></i>
    <span>fi-align-right</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-book"></i>
    <span>fi-book</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-layers"></i>
    <span>fi-layers</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-stack"></i>
    <span>fi-stack</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-stack-2"></i>
    <span>fi-stack-2</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-paper"></i>
    <span>fi-paper</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-paper-stack"></i>
    <span>fi-paper-stack</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-search"></i>
    <span>fi-search</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-zoom-in"></i>
    <span>fi-zoom-in</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-zoom-out"></i>
    <span>fi-zoom-out</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-reply"></i>
    <span>fi-reply</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-circle-plus"></i>
    <span>fi-circle-plus</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-circle-minus"></i>
    <span>fi-circle-minus</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-circle-check"></i>
    <span>fi-circle-check</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-circle-cross"></i>
    <span>fi-circle-cross</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-square-plus"></i>
    <span>fi-square-plus</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-square-minus"></i>
    <span>fi-square-minus</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-square-check"></i>
    <span>fi-square-check</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-square-cross"></i>
    <span>fi-square-cross</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-microphone"></i>
    <span>fi-microphone</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-record"></i>
    <span>fi-record</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-skip-back"></i>
    <span>fi-skip-back</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-rewind"></i>
    <span>fi-rewind</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-play"></i>
    <span>fi-play</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-pause"></i>
    <span>fi-pause</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-stop"></i>
    <span>fi-stop</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-fast-forward"></i>
    <span>fi-fast-forward</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-skip-forward"></i>
    <span>fi-skip-forward</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-shuffle"></i>
    <span>fi-shuffle</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-repeat"></i>
    <span>fi-repeat</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-folder"></i>
    <span>fi-folder</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-umbrella"></i>
    <span>fi-umbrella</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-moon"></i>
    <span>fi-moon</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-thermometer"></i>
    <span>fi-thermometer</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-drop"></i>
    <span>fi-drop</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-sun"></i>
    <span>fi-sun</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-cloud"></i>
    <span>fi-cloud</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-cloud-upload"></i>
    <span>fi-cloud-upload</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-cloud-download"></i>
    <span>fi-cloud-download</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-upload"></i>
    <span>fi-upload</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-download"></i>
    <span>fi-download</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-location"></i>
    <span>fi-location</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-location-2"></i>
    <span>fi-location-2</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-map"></i>
    <span>fi-map</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-battery"></i>
    <span>fi-battery</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-head"></i>
    <span>fi-head</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-briefcase"></i>
    <span>fi-briefcase</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-speech-bubble"></i>
    <span>fi-speech-bubble</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-anchor"></i>
    <span>fi-anchor</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-globe"></i>
    <span>fi-globe</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-box"></i>
    <span>fi-box</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-reload"></i>
    <span>fi-reload</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-share"></i>
    <span>fi-share</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-marquee"></i>
    <span>fi-marquee</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-marquee-plus"></i>
    <span>fi-marquee-plus</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-marquee-minus"></i>
    <span>fi-marquee-minus</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-tag"></i>
    <span>fi-tag</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-power"></i>
    <span>fi-power</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-command"></i>
    <span>fi-command</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-alt"></i>
    <span>fi-alt</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-esc"></i>
    <span>fi-esc</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-bar-graph"></i>
    <span>fi-bar-graph</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi--bargraph-2"></i>
    <span>fi--bargraph-2</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-pie-graph"></i>
    <span>fi-pie-graph</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-star"></i>
    <span>fi-star</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-arrow-left"></i>
    <span>fi-arrow-left</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-arrow-right"></i>
    <span>fi-arrow-right</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-arrow-up"></i>
    <span>fi-arrow-up</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-arrow-down"></i>
    <span>fi-arrow-down</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-volume"></i>
    <span>fi-volume</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-mute"></i>
    <span>fi-mute</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-content-right"></i>
    <span>fi-content-right</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-content-left"></i>
    <span>fi-content-left</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-grid"></i>
    <span>fi-grid</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-grid-2"></i>
    <span>fi-grid-2</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-columns"></i>
    <span>fi-columns</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-loader"></i>
    <span>fi-loader</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-bag"></i>
    <span>fi-bag</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-ban"></i>
    <span>fi-ban</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-flag"></i>
    <span>fi-flag</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-trash"></i>
    <span>fi-trash</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-expand"></i>
    <span>fi-expand</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-contract"></i>
    <span>fi-contract</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-maximize"></i>
    <span>fi-maximize</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-minimize"></i>
    <span>fi-minimize</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-plus"></i>
    <span>fi-plus</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-minus"></i>
    <span>fi-minus</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-check"></i>
    <span>fi-check</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-cross"></i>
    <span>fi-cross</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-move"></i>
    <span>fi-move</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-delete"></i>
    <span>fi-delete</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-menu"></i>
    <span>fi-menu</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-archive"></i>
    <span>fi-archive</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-inbox"></i>
    <span>fi-inbox</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-outbox"></i>
    <span>fi-outbox</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-file"></i>
    <span>fi-file</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-file-add"></i>
    <span>fi-file-add</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-file-subtract"></i>
    <span>fi-file-subtract</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-help"></i>
    <span>fi-help</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-open"></i>
    <span>fi-open</span> 
  </a>
</li>
<li>
  <a class="<?=($current == $m[1])? 'active':'';?>" href="<?=base_url()?>manage/<?=$m[1];?>">
    <i class="fi-ellipsis"></i>
    <span>fi-ellipsis</span> 
  </a>
</li>
</ul> -->