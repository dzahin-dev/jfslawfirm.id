<? $dbs = $this->db->get_where('top_banner',array('keyword'=>$keyword))->row();?>

<div class="col-lg-12">
    <div class="card ml_card">
        <div class="card-heading bg-inverse">
            <h3 class="card-title text-white">Top Banner [<?=$keyword?>]</h3>
        </div>
        <div class="card-body">
            <div class="tab-content">

                <?=form_open_multipart('banner/save/'.$keyword,array("class"=>"form-horizontal"))?>

                <div class="form-group row">
                    <label class="control-label col-md-2">Banner Image</label>
                    <div class="col-md-10">
                        <?php if (strlen($dbs->image) > 0): ?>
                            <a target="_blank" href="<?=base_url()?>media/banner/<?=$dbs->image?>">
                                <img width="150" src="<?=base_url()?>media/banner/<?=$dbs->image?>">
                            </a>
                        <?php endif ?>
                        <input type="file" class="default" name="userfile">
                        <p class="text-muted">* Image size up to 1280 x 800 PX , JPG & PNG allowed.</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-2">Title</label>
                    <div class="col-md-5">
                        <input type="text" class="form-control" name="title" value="<?=$dbs->title?>">
                    </div>
                </div>
                <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                <?=form_close()?>                                        
            </div>
        </div>
    </div>
</div>