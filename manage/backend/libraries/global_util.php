<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Global_util {

    function __construct(){
        $this->CI =& get_instance();
    }
	
    function upload($directory) {
    	$_FILES['userfile']['name'] = strtolower($_FILES['userfile']['name']);
        $config['upload_path']      = "../".$directory;
        $config['allowed_types']    = 'jpg|png';
        $config['max_size']         = '10000';
        $config['max_width']        = '5000';
        $config['max_height']       = '5000';
        $config['encrypt_name']     = true;
        $this->CI->load->library('upload', $config);
        if ( ! $this->CI->upload->do_upload()){
            return array(false,$this->CI->upload->display_errors());
        }else{
            $a = $this->CI->upload->data();
            $this->save_alt($a['file_name'],$_FILES['userfile']['name']);
            return array(true,$a);
        }
    }

    function upload_pdf($directory) {
        $_FILES['userfile']['name'] = strtolower($_FILES['userfile']['name']);
        $config['upload_path']      = "../".$directory;
        $config['allowed_types']    = 'pdf';
        $config['max_size']         = '10000';
        $config['encrypt_name']     = true;
        $this->CI->load->library('upload', $config);
        if ( ! $this->CI->upload->do_upload()){
            return array(false,$this->CI->upload->display_errors());
        }else{
            $a = $this->CI->upload->data();
            $this->save_alt($a['file_name'],$_FILES['userfile']['name']);
            return array(true,$a);
        }
    }

    function upload_all($directory) {
        $_FILES['userfile']['name'] = strtolower($_FILES['userfile']['name']);
        $config['upload_path']      = "../".$directory;
        $config['allowed_types']    = '*';
        $config['max_size']         = '10000';
        $config['max_width']        = '5000';
        $config['max_height']       = '5000';
        $config['encrypt_name']     = true;
        $this->CI->load->library('upload', $config);
        if ( ! $this->CI->upload->do_upload()){
            return array(false,$this->CI->upload->display_errors());
        }else{
            $a = $this->CI->upload->data();
            $this->save_alt($a['file_name'],$_FILES['userfile']['name']);
            return array(true,$a);
        }
    }

    function save_alt($a,$b){
        // $data['img']   = $a;
        // $data['alt']   = $b;
        // $this->CI->db->insert('alt_image',$data);
    }

    function upload_name($directory,$input){
        $_FILES[$input]['name']     = strtolower($_FILES[$input]['name']);
        $config['upload_path']      = "../".$directory;
        $config['allowed_types']    = 'jpg|png';
        $config['max_size']         = '10000';
        $config['max_width']        = '5000';
        $config['max_height']       = '5000';
        $config['encrypt_name']     = true;
        $this->CI->load->library('upload', $config);
        if ( ! $this->CI->upload->do_upload($input)){
            return "";
        }else{
            $a = $this->CI->upload->data();
            $this->save_alt($a['file_name'],$_FILES[$input]['name']);
            return $a['file_name'];
        }
    }

    function froala_remove_license($text){
        $s = '<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>';
        return str_replace($s, "", $text);
    }
}