<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "news";
	}
	function index(){
		$data['news']			= $this->db->order_by('id','desc')->get('news');
		$data['local_view'] 	= 'v_news_index';
		$this->load->view('v_manage',$data);
	}

	function add(){
		if (is_post()) {
			$data['image'] = $this->set_image()[0];
			$this->db->insert('news',$data);

			$id   		= $this->db->insert_id();
			redirect(base_url()."manage/news/edit/$id");
		}

		$data['local_view'] 	= 'v_news_add';
		$this->load->view('v_manage',$data);
	}

	function edit($id){
		$data['news_id']		= $id;
		$data['news']			= $this->db->get_where('news',array('id'=>$id))->row();
		$data['local_view'] 	= 'v_news_edit';
		$this->load->view('v_manage',$data);
	}

	function update_images($id){
		if (is_post()) {
			$img		= $this->set_image();
			
			$this->db->where('id', $id);
			$this->db->update('news', array('image'=>$img[0],'image_alt'=>$img[1])); 
			
			$this->session->set_flashdata('message','Data saved successfully');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function update_content($news_id){
		$data = $this->input->post();

		$this->db->where('id', $news_id);
		$this->db->update('news', $data);

		$this->session->set_flashdata('message','Data saved successfully');
		redirect(base_url()."manage/news/edit/$news_id");
	}

	function delete($id){
		$this->db->delete('news', array('id' => $id)); 
		$this->session->set_flashdata('message','Data deleted successfully');
		redirect($_SERVER['HTTP_REFERER']);
	}

	function set_active(){
		$input = $this->input->post();
		$this->db->where('id', $input['id']);
		$this->db->update('news', array('active'=>$input['status'])); 
	}

	function set_image(){
		$img = $this->global_util->upload_all('media/news');
		if ($img[0] == 1) {
			return array($img[1]['file_name'],$img[1]['orig_name']);
		}else return '';
	}

	function upload_mce(){
		$b['location'] = base_url().'media/news/'.$this->set_image()[0];
		echo json_encode($b);	
	}
}