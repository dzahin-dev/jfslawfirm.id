<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage News</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">News List</h3>
                        <div class="btn-group">
                            <a class="btn btn-success waves-effect" href="<?=base_url('manage/news/add')?>">
                                <i class="fa fa-plus"></i>
                                add news
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped mb-0">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Active</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Content</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=0; foreach ($news->result() as $d): $i++;?>
                                    <tr>
                                        <th scope="row"><?=$i?></th>
                                        <td>
                                            <input data-id="<?=$d->id?>" class="checkcolor" type="checkbox" data-plugin="switchery" data-color="#7FBF7F"  <?=($d->active == 1)?'checked':'';?>/>
                                        </td>
                                        <td>
                                            <a target="_blank" href="<?=base_url()?>media/news/<?=$d->image?>">
                                                <img style="width:100px;" src="<?=base_url()?>media/news/<?=$d->image?>">
                                            </a>
                                        <td><?=$d->title?></td>
                                        <td><?=smart_trim($d->title,100)?></td>
                                        <td class="action">
                                            <a title="edit news" href="<?=base_url()?>manage/news/edit/<?=$d->id?>" class="edit fa fa-pencil delete-list dz-tip"></a>
                                            <a title="delete news" href="<?=base_url()?>manage/news/delete/<?=$d->id?>" class="delete fa fa-times-circle delete-list dz-tip confirm-delete"></a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('[data-plugin="switchery"]').each(function (idx, obj) {
            new Switchery($(this)[0], $(this).data());
        });

        $('.checkcolor').change(function() {
            var dataf = {};
            var gbl = $("#global-form").find('input');
            document.body.style.cursor='wait';
            var fstatus = 0;
            var fid     = $(this).data('id');
            if($(this).is(":checked")) 
                fstatus = 1;

            dataf['status']         = fstatus;
            dataf['id']             = fid;
            dataf[gbl.attr('name')] = gbl.val();

            $.post("<?=base_url();?>manage/news/set_active", dataf).done(function( data ) {
                document.body.style.cursor='default';
            });
        });
    })
</script>