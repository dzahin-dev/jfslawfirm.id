<?=mce5('mce_news',base_url('manage/news/upload_mce'));?>
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage News</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">News Edit</h3>
                        <div class="btn-group">
                            <a class="btn btn-success waves-effect" href="<?=base_url()?>manage/news">
                                <i class="fa fa-arrow-left"></i>
                                news list
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Images News</h4>
                        <br>
                        <?=form_open_multipart('news/update_images/'.$news_id,array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="control-label col-md-2">image news</label>
                                <div class="col-md-10">
                                    <a target="_blank" href="<?=base_url()?>media/news/<?=$news->image?>">
                                        <img width="100" src="<?=base_url()?>media/news/<?=$news->image?>">
                                    </a>
                                    <input type="file" class="default" name="userfile">
                                    <p class="text-muted m-b-25">* Image size up to 800 x 500 PX , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                        <?=form_close()?>
                        <hr>
                        <br>
                        <h4 class="header-title m-t-0">News Content</h4><br>
                        <?=form_open_multipart('news/update_content/'.$news_id,array("class"=>"form-horizontal"))?>
                        <div class="form-group row">
                            <label class="control-label col-md-2">Title</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="title" value="<?=$news->title?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-2">TAG / Category</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="categories" value="<?=$news->categories?>">
                                <small>separate with comma</small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-2">Meta Keyword</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="meta_keyword" value="<?=$news->meta_keyword?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-2">Meta Description</label>
                            <div class="col-md-7">
                                <textarea name="meta_description" class="form-control"><?=$news->meta_description?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 control-label">Content</label>
                            <div class="col-md-10">
                                <textarea class="selector mce_news" style="margin-top: 30px;" placeholder="Type some text" name="content"><?=$news->content?></textarea>
                            </div>
                        </div>                                            
                        <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                        <?=form_close()?>                                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>