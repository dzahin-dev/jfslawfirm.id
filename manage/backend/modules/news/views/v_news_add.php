<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage News</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">News Add</h3>
                        <div class="btn-group">
                            <a class="btn btn-success waves-effect" href="<?=base_url()?>manage/news">
                                <i class="fa fa-arrow-left"></i>
                                news list
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <br>
                        <?=form_open_multipart('news/add',array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="control-label col-md-2">news image</label>
                                <div class="col-md-10">
                                    <input type="file" class="default" name="userfile">
                                    <p class="text-muted m-b-25">* Image size up to 300 x 500 PX , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
