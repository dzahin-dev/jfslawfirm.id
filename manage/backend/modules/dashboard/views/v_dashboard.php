<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h4 style="line-height:30px" class="page-title pull-left">Dashboard</h4>
					<!-- <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;" class="pull-right">
						<i class="fa fa-calendar"></i>&nbsp;
						<span></span> <i class="fa fa-caret-down"></i>
					</div>	 -->
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div id="main-load"></div>
		<div 
			id="loader-bar"
			style="width: 50%;margin: 0 auto;height: 15px;border-radius: 10px;margin-top: 20px;max-width: 250px;" 
			class="progress-bar bg-info progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"
		>
	</div>
</div>

<script type="text/javascript">
	var GLBsd 	= "<?=$this->session->userdata('session_subdomain');?>";
	var start 	= moment().startOf('month');
	var end 	= moment().endOf('month');
	$(function(){

		if (GLBsd.length == 0) {
			$.get("<?=base_url('manage/login/set_default_subdomain')?>", function(e){
				load_dashboard(start.format('YYYY-MM-DD'),end.format('YYYY-MM-DD'));
			});	
		}else load_dashboard(start.format('YYYY-MM-DD'),end.format('YYYY-MM-DD'));

		CLOG = start;

		function cb(start, end) {
			$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		}

		$('#reportrange').daterangepicker({
			startDate: moment().startOf('month'),
			endDate: moment().endOf('month'),
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
		}, cb);

		cb(start, end);

		$('#reportrange').on('apply.daterangepicker', function(ev, picker) {
			var ad = picker.startDate.format('YYYY-MM-DD');
			var bd = picker.endDate.format('YYYY-MM-DD');

			load_dashboard(ad,bd);
		});
	})

	function load_dashboard(s,e){
		var url = "<?=base_url()?>manage/dashboard/load/"+s+"s"+e;
		$("#main-load").html("");
		$("#loader-bar").show();
		$.get( url, function( data ) {
			$("#main-load").html(data);
			$("#loader-bar").hide();
		});
	}
</script>