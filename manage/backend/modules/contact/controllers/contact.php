<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "category";
	}
	function index(){

		$data['c'] 			= $this->db->get('contact')->row();

		$data['local_view'] 	= 'v_contact';
		$this->load->view('v_manage',$data);
	}

	function save(){
		$data 	= $this->input->post();

		$this->db->where('id', 1);
		$this->db->update('contact', $data);

		$this->session->set_flashdata('message','Data saved successfully');
		redirect(base_url('manage/contact'));
	}

	function save_bg(){
		$img = $this->global_util->upload('media/images');
		$data['brochure_bg'] 		= $img[1]['file_name'];

		$this->db->where('id', 1);
		$this->db->update('contact', $data);

		$this->session->set_flashdata('message','Data saved successfully');
		redirect(base_url('manage/contact'));
	}

	function save_brochure(){
		$pdf = $this->global_util->upload_pdf('media/files');
		$data['brochure_link'] 		= $pdf[1]['file_name'];

		$this->db->where('id', 1);
		$this->db->update('contact', $data);

		$this->session->set_flashdata('message','Data saved successfully');
		redirect(base_url('manage/contact'));
	}

	function form(){
		$data['f']			= $this->db->order_by('id desc')->get('contact_form');
		$data['local_view'] = 'v_form';
		$this->load->view('v_manage',$data);		
	}
}