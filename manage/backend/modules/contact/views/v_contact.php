<div class="content">
    <div class="container-fluid">
        
        <?$this->load->view('i_title',array('tt'=>'Manage Contact Us','act'=>'contact'))?>
        
        <div class="row">

            <?=$this->load->view('include/top_banner',array('keyword'=>'contact'));?>
            
            <div class="col-lg-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Contact Content</h3>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">

                            <?=form_open_multipart('contact/save',array("class"=>"form-horizontal"))?>

                            <div class="form-group row">
                                <label class="control-label col-md-2">Map IFRAME</label>
                                <div class="col-md-7">
                                    <textarea name="map" class="form-control"><?=$c->map?></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <h3>Contact Info</h3>
                                    <input type="text" name="office_1" class="form-control mb-2" placeholder="line 1" value="<?=$c->office_1?>">
                                    <input type="text" name="address_1" class="form-control mb-2" placeholder="line 2" value="<?=$c->address_1?>">
                                    <input type="text" name="telp_1" class="form-control mb-2" placeholder="line 3" value="<?=$c->telp_1?>">
                                </div>
                                <div class="col-lg-4">
                                    <h3>Address</h3>
                                    <input type="text" name="office_2" class="form-control mb-2" placeholder="line 1" value="<?=$c->office_2?>">
                                    <input type="text" name="address_2" class="form-control mb-2" placeholder="line 2" value="<?=$c->address_2?>">
                                    <input type="text" name="telp_2" class="form-control mb-2" placeholder="line 3" value="<?=$c->telp_2?>">
                                </div>
                                <div class="col-lg-4">
                                    <h3>Schedule</h3>
                                    <input type="text" name="office_3" class="form-control mb-2" placeholder="line 1" value="<?=$c->office_3?>">
                                    <input type="text" name="address_3" class="form-control mb-2" placeholder="line 2" value="<?=$c->address_3?>">
                                    <input type="text" name="telp_3" class="form-control mb-2" placeholder="line 3" value="<?=$c->telp_3?>">
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                            <?=form_close()?>                                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>