<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title"><?=$tt?></h4>
            <div class="btn-group" style="float: right;">
                <a class="btn btn-sm btn-secondary <?=($act=='contact')? 'active' : '' ;?> waves-effect" href="<?=base_url()?>manage/contact">
                    Contact Page
                </a>
                <a class="btn btn-sm btn-secondary <?=($act=='form')? 'active' : '' ;?> waves-effect" href="<?=base_url()?>manage/contact/form">
                    Contact Form
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>