<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage Gallery</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
        <?=$this->load->view('include/top_banner',array('keyword'=>'gallery'));?>

            <div class=" col-xl-5 col-lg-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Upload Gallery</h3>
                    </div>
                    <div class="card-body">
                        <br>
                        <?= form_open_multipart('gallery/save_content', array("class" => "form-horizontal")) ?>
                        <div class="form-group row">
                            <label class="control-label col-md-12">Image</label>
                            <div class="col-md-12">
                                <input type="file" class="default" name="userfile">
                                <p class="text-muted">* Image size best fit to 800 x 600 PX , JPG & PNG allowed.</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-12">Title</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="title">
                            </div>
                        </div>
                        <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                        <?= form_close() ?>
                    </div>
                </div>
            </div>

            <div class=" col-xl-7 col-lg-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">List Images</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <?php foreach ($gallery->result() as $g) : ?>
                                <div class="col-lg-3">
                                    <div class="thumbnail mb-3">
                                        <a target="_blank" href="<?= base_url() ?>media/gallery/<?= $g->image ?>">
                                            <img src="<?= base_url() ?>media/gallery/<?= $g->image ?>" alt="" class="img-fluid d-block">
                                            <span><?=smart_trim($g->title,50)?></span>
                                        </a>
                                        <a href="<?= base_url() ?>manage/gallery/delete/<?= $g->id ?>" class="confirm-delete btn btn-danger waves-effect waves-light gallery-delete" role="button">delete</a>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>