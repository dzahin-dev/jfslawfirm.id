<style type="text/css">
    .gallery-delete{
        margin: 0 auto;
        position: absolute;
        top: 50%;
        left: 50%;
        margin-left: -35px;
        margin-top: -20px;
        display: none;
    }
    .thumbnail:hover a{
        display: block;
    }
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage Gallery Instagram Footer</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Instagram Gallery</h3>
                    </div>
                    <div class="card-body">
                        <br>
                        <?=form_open_multipart('gallery/ig_save_content',array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="control-label col-md-2">Image</label>
                                <div class="col-md-10">
                                    <input type="file" class="default" name="userfile">
                                    <p class="text-muted">* Image size up to 480 x 320 PX , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">Link</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="link">
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                        <?=form_close()?>                                        

                        <hr>
                        <h4>List Image</h4>
                        <b>Only last 6 images will show in front end</b><br><br>

                        <div class="row">
                            <?php foreach ($gallery->result() as $g): ?>
                                <div class="col-lg-3">
                                    <div class="thumbnail mb-3">
                                        <a target="_blank" href="<?=$g->link?>">
                                            <img src="<?=base_url()?>media/gallery/<?=$g->image?>" alt="" class="img-fluid d-block">
                                        </a>
                                        <a href="<?=base_url()?>manage/gallery/ig_delete/<?=$g->id?>" class="confirm-delete btn btn-danger waves-effect waves-light gallery-delete" role="button">delete</a>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>