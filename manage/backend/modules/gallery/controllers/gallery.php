<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "category";
	}
	function index(){
		$data['gallery']		= $this->db->order_by('id','desc')->get('gallery');
		$data['local_view'] 	= 'v_gallery';
		$this->load->view('v_manage',$data);
	}

	function save_content(){
		$data = $this->input->post();

        $img = $this->global_util->upload_all('media/gallery');
		if ($img[0] == 1) {
			$data['image'] = $img[1]['file_name'];
			$this->db->insert('gallery',$data);
			$this->session->set_flashdata('message','Data saved successfully');	
		}else $this->session->set_flashdata('message','Error, data not saved');
		redirect(base_url()."manage/gallery");
	}

	function delete($id){
		$this->db->delete('gallery', array('id'=>$id));
		$this->session->set_flashdata('message','Data deleted successfully');
		redirect(base_url()."manage/gallery");
	}

	// IG

	function ig(){
		$data['gallery']		= $this->db->order_by('id','desc')->get('gallery_ig');
		$data['local_view'] 	= 'v_gallery_ig';
		$this->load->view('v_manage',$data);
	}

	function ig_save_content(){
		$data = $this->input->post();

		$img = $this->global_util->upload_all('media/gallery');
		if ($img[0] == 1) {
			$data['image'] = $img[1]['file_name'];
			$this->db->insert('gallery_ig',$data);
			$this->session->set_flashdata('message','Data saved successfully');	
		}else $this->session->set_flashdata('message','Error, data not saved');
		redirect(base_url()."manage/gallery/ig");
	}

	function ig_delete($id){
		$this->db->delete('gallery_ig', array('id'=>$id));
		$this->session->set_flashdata('message','Data deleted successfully');
		redirect(base_url()."manage/gallery/ig");
	}
}