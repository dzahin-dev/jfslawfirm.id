<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	function save($key){
		$data 	= $this->input->post();
		$img 	= $this->set_image();
		
		if ($img != '') {
			$data['image'] 		= $img[1]['file_name'];
			$data['image_alt'] 	= $img[1]['orig_name'];
		}

		$this->db->where('keyword', $key);
		$this->db->update('top_banner', $data);

		$this->session->set_flashdata('message','Data saved successfully');
		redirect($_SERVER['HTTP_REFERER']);
	}

	function set_image(){
		$img = $this->global_util->upload_all('media/banner');
		if ($img[0] == 1) {
			return $img;
		}else return '';
	}
}