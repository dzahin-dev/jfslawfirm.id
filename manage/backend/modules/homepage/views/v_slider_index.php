<div class="content">
    <div class="container-fluid">

        <?$this->load->view('i_title',array('tt'=>'Manage Slider','act'=>'slider'))?>

        <div class="row">
            <div class="col-lg-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Slider List</h3>
                        <div class="btn-group">
                            <a class="btn btn-success waves-effect" href="<?=base_url()?>manage/homepage/slider/add">
                                add slider
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped mb-0">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Active</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Text & Button</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=0; foreach ($slider->result() as $d): $i++;?>
                                    <tr>
                                        <th scope="row"><?=$i?></th>
                                        <td>
                                            <input data-id="<?=$d->id?>" class="checkcolor" type="checkbox" data-plugin="switchery" data-color="#7FBF7F"  <?=($d->active == 1)?'checked':'';?>/>
                                        </td>
                                        <td>
                                            <a target="_blank" href="<?=base_url()?>media/slider/<?=$d->image?>">
                                                <img style="width:100px;" src="<?=base_url()?>media/slider/<?=$d->image?>">
                                            </a>
                                        </td>
                                        <td>
                                            <p><?=$d->text;?></p>
                                            <?php if ($d->button_text != ""): ?>
                                                <a target="_blank" href="<?=$d->button_url;?>">
                                                    <?=$d->button_text;?>
                                                </a>
                                            <?php endif ?>
                                        </td>
                                        <td class="action">
                                            <a title="edit slider" href="<?=base_url()?>manage/homepage/slider/edit/<?=$d->id?>" class="edit fa fa-pencil delete-list dz-tip"></a>
                                            <a title="delete slider" href="<?=base_url()?>manage/homepage/slider/delete/<?=$d->id?>" class="delete fa fa-times-circle delete-list dz-tip confirm-delete"></a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('[data-plugin="switchery"]').each(function (idx, obj) {
            new Switchery($(this)[0], $(this).data());
        });

        $('.checkcolor').change(function() {
            var dataf = {};
            var gbl = $("#global-form").find('input');
            document.body.style.cursor='wait';
            var fstatus = 0;
            var fid     = $(this).data('id');
            if($(this).is(":checked")) 
                fstatus = 1;

            dataf['status']         = fstatus;
            dataf['id']             = fid;
            dataf[gbl.attr('name')] = gbl.val();

            $.post("<?=base_url();?>manage/homepage/slider/set_active", dataf).done(function( data ) {
                document.body.style.cursor='default';
            });
        });
    })
</script>