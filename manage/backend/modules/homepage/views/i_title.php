<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title"><?=$tt?></h4>
            <div class="btn-group" style="float: right;">
                <a class="btn btn-sm btn-secondary <?=($act=='home')? 'active' : '' ;?> waves-effect" href="<?=base_url()?>manage/homepage">
                    Home Texting
                </a>
                <a class="btn btn-sm btn-secondary <?=($act=='practice')? 'active' : '' ;?> waves-effect" href="<?=base_url()?>manage/homepage/practice">
                    Practice Area
                </a>
                <a class="btn btn-sm btn-secondary <?=($act=='slider')? 'active' : '' ;?> waves-effect" href="<?=base_url()?>manage/homepage/slider">
                    Slider
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>