<div class="content">
    <div class="container-fluid">

        <?$this->load->view('i_title',array('tt'=>'Manage Slider','act'=>'slider'))?>

        <div class="row">
            <div class="col-lg-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Slider Edit</h3>
                        <div class="btn-group">
                            <a class="btn btn-success waves-effect" href="<?=base_url()?>manage/homepage/slider">
                                <i class="fa fa-arrow-left"></i>
                                slider list
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Images Slider</h4>
                        <br>
                        <?=form_open_multipart('homepage/slider/update_images/'.$slider_id,array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="control-label col-md-2">image slider</label>
                                <div class="col-md-10">
                                    <a target="_blank" href="<?=base_url()?>media/slider/<?=$slider->image?>">
                                        <img width="100" src="<?=base_url()?>media/slider/<?=$slider->image?>">
                                    </a>
                                    <input type="file" class="default" name="userfile">
                                    <p class="text-muted m-b-25">* Image size up to 1280 x 800 PX , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">Text</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="text" value="<?=$slider->text;?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">Button Text</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="button_text" value="<?=$slider->button_text;?>" >
                                    <p class="text-muted m-b-25">* leave it blank, to hide the button.</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">Button Link</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="button_url" value="<?=$slider->button_url;?>" >
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                        <?=form_close()?>
                        <hr>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
