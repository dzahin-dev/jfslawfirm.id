<div class="content">
    <div class="container-fluid">

        <?$this->load->view('i_title',array('tt'=>'Manage Homepage Texting','act'=>'home'))?>

        <div class="row">
            <div class="col-sm-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Top Jargon Text</h3>
                    </div>
                    <div class="card-body">
                        <?=form_open('homepage/save_text/',array("class"=>"form-horizontal"))?>
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="simpleinput">Jargon</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="jargon" value="<?=$h->jargon?>">
                                    </div>
                                </div>
                                <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Section About</h3>
                    </div>
                    <div class="card-body">
                        <?=form_open('homepage/save_text/',array("class"=>"form-horizontal"))?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="simpleinput">About Title 1</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control" name="about_title_1" value="<?=$h->about_title_1?>">
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control" name="about_title_2" value="<?=$h->about_title_2?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="simpleinput">About Short Description</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" name="about_description"><?=$h->about_description?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="simpleinput">About Image</label>
                                    <div class="col-md-10">
                                        <div class="member-avatar avatar-xl mx-auto d-block mb-3">
                                            <a target="_blank" href="<?=base_url()?>media/images/<?=$h->about_image?>">
                                                <img width="100" src="<?=base_url()?>media/images/<?=$h->about_image?>" class="img-thumbnail" alt="profile-image">    
                                            </a>
                                            <span>change picture</span>
                                            <input type="file" name="userfile" data-field_image="about_image" data-field_image_alt="about_image" class="auto-submit-file">
                                        </div>    
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="simpleinput">Button</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control" name="about_button_text" value="<?=$h->about_button_text?>">
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control" name="about_button_url" value="<?=$h->about_button_url?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                    </div>
                    <?=form_close()?>
                    <?=form_open_multipart('homepage/save_image',array("id"=>"submit-upload"))?>
                    <input type="hidden" name="field" id="field_image">
                    <input type="hidden" name="field_alt" id="field_image_alt">
                    <?=form_close()?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">News Area</h3>
                    </div>
                    <div class="card-body">
                        <?=form_open('homepage/save_text/',array("class"=>"form-horizontal"))?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="simpleinput">News Title</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control" name="news_title_1" value="<?=$h->news_title_1?>">
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control" name="news_title_2" value="<?=$h->news_title_2?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="simpleinput">News Description</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" name="news_description"><?=$h->news_description?></textarea>
                                    </div>
                                </div>
                                <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>                
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    $(function(){
        $(".auto-submit-file").change(function(){
            $("#submit-upload").append($(this));
            $("#field_image").val($(this).data('field_image'));
            $("#field_image_alt").val($(this).data('field_image_alt'));
            $("#submit-upload").submit();
        });

        var last_valid_selection = null;

        $('#userRequest_activity').change(function(event) {
            if ($(this).val().length > 4) {
                $(this).val(last_valid_selection);
            } else {
                last_valid_selection = $(this).val();
            }
        });
    })
</script>