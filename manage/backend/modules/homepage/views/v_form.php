<div class="content">
    <div class="container-fluid">

        <?$this->load->view('i_title',array('tt'=>'User Form','act'=>'form'))?>

        <div class="row">
            <div class="col-lg-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Question List</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped mb-0">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=0; foreach ($f->result() as $d): $i++;?>
                                    <tr>
                                        <td><?=$i?></td>
                                        <td><?=$d->name?></td>
                                        <td><?=$d->email?></td>
                                        <td><?=$d->phone?></td>
                                        <td><?=pretty_date($d->submit_date)?></td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>