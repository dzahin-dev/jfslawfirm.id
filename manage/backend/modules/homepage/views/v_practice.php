<div class="content">
    <div class="container-fluid">

        <?$this->load->view('i_title',array('tt'=>'Manage Homepage Texting','act'=>'practice'))?>        

        <div class="row">
            <div class="col-sm-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Section Practice Area</h3>
                    </div>
                    <div class="card-body">
                        <?=form_open('homepage/save_text/',array("class"=>"form-horizontal"))?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="simpleinput">Practice Area Title</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control" name="practice_title_1" value="<?=$h->practice_title_1?>">
                                    </div>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control" name="practice_title_2" value="<?=$h->practice_title_2?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="simpleinput">Practice Area Short Description</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" name="practice_description"><?=$h->practice_description?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                        <?=form_close()?>
                        <hr>
                        <div class="row">
                            <div class="col-sm-12"><h5>List Practice Area</h5></div>
                            <div class="col-sm-5">
                                <?=form_open_multipart('homepage/save_pa/',array("class"=>"form-horizontal"))?>
                                    <div class="form-group row">
                                        <label class="col-md-5 col-form-label" for="simpleinput">Image</label>
                                        <div class="col-md-7">
                                            <input type="file" name="userfile" data-field_image="about_image" data-field_image_alt="about_image"><br>
                                            <small> * image size 370 x 220 px</small>
                                        </div>
                                        <label class="col-md-12 col-form-label" for="simpleinput">Title</label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" name="title" value="">
                                        </div>
                                        <label class="col-md-12 col-form-label" for="simpleinput">Description</label>
                                        <div class="col-md-12">
                                            <textarea class="form-control" name="description" name="title"></textarea>
                                        </div>
                                    </div>
                                    <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                                <?=form_close()?>
                            </div>
                            <div class="col-sm-7">
                                <div class="form-group row">
                                    <table class="table table-striped mb-0">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Image</th>
                                                <th scope="col">Title - Description</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=0; foreach ($pa->result() as $d): $i++;?>
                                            <tr>
                                                <th scope="row"><?=$i?></th>
                                                <td>
                                                    <a target="_blank" href="<?=base_url()?>media/images/<?=$d->image?>">
                                                        <img style="width:100px;" src="<?=base_url()?>media/images/<?=$d->image?>">
                                                    </a>
                                                    <td>
                                                        <b><?=$d->title?></b><br>
                                                        <?=$d->description?>
                                                    </td>
                                                    <td class="action">
                                                        <a title="delete" href="<?=base_url()?>manage/homepage/delete_pa/<?=$d->id?>" class="delete fa fa-times-circle delete-list dz-tip confirm-delete"></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Process Area</h3>
                    </div>
                    <div class="card-body">
                        <?=form_open_multipart('homepage/save_proc/',array("class"=>"form-horizontal"))?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="simpleinput">Process Title</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="proc_title" value="<?=$h->proc_title?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="simpleinput">Image</label>
                                    <div class="col-md-7">
                                        <?php if ($h->proc_image): ?>
                                            <a target="_blank" href="<?=base_url()?>media/images/<?=$h->proc_image?>">
                                                <img width="100" src="<?=base_url()?>media/images/<?=$h->proc_image?>" class="img-thumbnail" alt="profile-image">    
                                            </a>
                                        <?php endif ?>
                                        <input type="file" name="userfile" data-field_image="about_image"><br>
                                        <small> * image size 1920 x 380 px</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label" for="simpleinput">Process Description</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" name="proc_description"><?=$h->proc_description?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <?php for ($i=1;$i<7;$i++): ?>
                                <div class="col-sm-4">
                                    <div class="form-group row">
                                        <label class="col-md-2 col-form-label" for="simpleinput">
                                            <h1>0<?=$i?></h1>
                                        </label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" value="<?=$pj['proc_json_title'][$i]?>" name="proc_json_title[<?=$i?>]">
                                            <br>
                                            <textarea class="form-control" name="proc_json_description[<?=$i?>]"><?=$pj['proc_json_description'][$i]?></textarea>
                                        </div>
                                    </div>
                                </div>
                            <?php endfor ?>
                            <div class="col-sm-12">
                                <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>                
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $(".auto-submit-file").change(function(){
            $("#submit-upload").append($(this));
            $("#field_image").val($(this).data('field_image'));
            $("#field_image_alt").val($(this).data('field_image_alt'));
            $("#submit-upload").submit();
        });

        var last_valid_selection = null;

        $('#userRequest_activity').change(function(event) {
            if ($(this).val().length > 4) {
                $(this).val(last_valid_selection);
            } else {
                last_valid_selection = $(this).val();
            }
        });
    })
</script>