<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homepage extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "homepage";
	}
	
	function index(){
		$data['h']			= $this->db->get('home_texting')->row();
		$data['pa']			= $this->db->get('home_pa');
		$data['local_view'] = 'v_homepage';
		$data['pj'] 		= json_decode($data['h']->proc_json,true);
		$this->load->view('v_manage',$data);		
	}

	function practice(){
		$data['h']			= $this->db->get('home_texting')->row();
		$data['pa']			= $this->db->get('home_pa');
		$data['local_view'] = 'v_practice';
		$data['pj'] 		= json_decode($data['h']->proc_json,true);
		$this->load->view('v_manage',$data);		
	}

	function save_text(){
		$input = $this->input->post();
		unset($input['userfile']);

		$this->db->where('id', 1);
		$this->db->update('home_texting',$input);

		$this->session->set_flashdata('message','Data saved successfully');
		redirect($_SERVER['HTTP_REFERER']);
	}

	function save_proc(){
		$input = $this->input->post();
		$data['proc_title'] = $input['proc_title'];
		$data['proc_description'] = $input['proc_description'];
		unset($input['proc_title']);unset($input['proc_description']);

		$img = $this->global_util->upload('media/images');
		if($img[0])
			$data['proc_image'] = $img[1]['file_name'];

		$data['proc_json'] = json_encode($input);

		$this->db->where('id', 1);
		$this->db->update('home_texting',$data);
		$this->session->set_flashdata('message','Data saved successfully');
		redirect($_SERVER['HTTP_REFERER']);
	}

	function save_image(){
		$img = $this->global_util->upload('media/images');
		$field 		= $this->input->post('field');

		$data[$field] 		= $img[1]['file_name'];
		$this->db->where('id', 1);

		$this->db->update('home_texting',$data);

		$this->session->set_flashdata('message','Data saved successfully');
		redirect(base_url()."manage/homepage");
	}

	function save_pa(){
		$img = $this->global_util->upload('media/images');
		if($img[0]){
			$data 			= $this->input->post();
			$data['image'] = $img[1]['file_name'];

			$this->db->insert('home_pa',$data);	

			$this->session->set_flashdata('message','Data saved successfully');	
		}else{
			$this->session->set_flashdata('message','Failed upload images');	
		}
		
		redirect($_SERVER['HTTP_REFERER']);
	}

	function delete_pa($id){
		$this->db->delete('home_pa', array('id' => $id)); 
		$this->session->set_flashdata('message','Data deleted successfully');
		redirect($_SERVER['HTTP_REFERER']);
	}

	function save_product(){
		$input = $this->input->post();
		$input['product_showcase'] = implode(',', $input['product_showcase']);
		
		$this->db->where('id', 1);
		$this->db->update('home_texting',$input);

		$this->session->set_flashdata('message','Data saved successfully');
		redirect(base_url()."manage/homepage");
	}

	function get_cek_product($d){
		$b = explode(",",$d->product_showcase);
		$a = array();
		foreach ($b as $p) {
			$a[$p] = 1;
		}
		return $a;
	}

	function form(){
		$data['f']			= $this->db->order_by('id desc')->get('home_slider_form');
		$data['local_view'] = 'v_form';
		$this->load->view('v_manage',$data);		
	}
}