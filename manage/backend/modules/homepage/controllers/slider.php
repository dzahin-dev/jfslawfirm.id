<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slider extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "homepage";
	}
	function index(){
		$data['slider']			= $this->db->order_by('id','desc')->get('home_slider');
		$data['local_view'] 	= 'v_slider_index';
		$this->load->view('v_manage',$data);		
	}

	function add(){
		if (is_post()) {
			$img		= $this->global_util->upload('media/slider','image');

			if($img[0]){
				$data = $this->input->post();

				$data['image'] 		= $img[1]['file_name'];
				$data['image_alt'] 	= $img[1]['orig_name'];
				
				$this->db->insert('home_slider',$data);	
				$this->session->set_flashdata('message','Data saved successfully');
				redirect(base_url()."manage/homepage/slider");
			}else{
				$this->session->set_flashdata('message','Upload Failed');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}

		$data['local_view'] 	= 'v_slider_add';
		$this->load->view('v_manage',$data);
	}

	function edit($id){
		$data['slider_id']		= $id;
		$data['slider']			= $this->db->get_where('home_slider',array('id'=>$id))->row();
		$data['local_view'] 	= 'v_slider_edit';
		$this->load->view('v_manage',$data);
	}

	function update_images($id){
		if (is_post()) {
			$img		= $this->global_util->upload('media/slider','img_header');

			$data = $this->input->post();

			if($img[0]){
				$data['image'] 		= $img[1]['file_name'];
				$data['image_alt'] 	= $img[1]['orig_name'];	
			}

			$this->db->where('id', $id);
			$this->db->update('home_slider', $data); 
			
			$this->session->set_flashdata('message','Data saved successfully');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function delete($id){
		$this->db->delete('home_slider', array('id' => $id)); 
		$this->session->set_flashdata('message','Data deleted successfully');
		redirect($_SERVER['HTTP_REFERER']);
	}

	function get_data_slider_lang($lang_id){
		$lang = $this->db->get_where('home_slider_lang',array('lang_id'=>$lang_id));
		$data = array();
		foreach ($lang->result() as $d) {
			$data[$d->home_slider_id] = $d;
		}
		return $data;
	}

	function get_data_slider_lang_id($slider_id){
		$lang = $this->db->get_where('home_slider_lang',array('home_slider_id'=>$slider_id));
		$data = array();
		foreach ($lang->result() as $d) {
			$data[$d->lang_id] = $d;
		}
		return $data;
	}

	function set_active(){
		$input = $this->input->post();
		$this->db->where('id', $input['id']);
		$this->db->update('home_slider', array('active'=>$input['status']));
	}
}