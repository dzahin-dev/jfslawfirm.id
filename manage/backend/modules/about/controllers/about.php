<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "category";
	}
	function index(){

		$data['ab'] 			= $this->db->get('about')->row();

		$data['local_view'] 	= 'v_about';
		$this->load->view('v_manage',$data);
	}

	function save(){
		$data 	= $this->input->post();
		$img 	= $this->set_image();
		
		if ($img != '') {
			$data['image'] 		= $img[1]['file_name'];
			$data['image_alt'] 	= $img[1]['orig_name'];
		}

		$this->db->where('id', 1);
		$this->db->update('about', $data);

		$this->session->set_flashdata('message','Data saved successfully');
		redirect(base_url('manage/about'));
	}

	function set_image(){
		$img = $this->global_util->upload_all('media/banner');
		if ($img[0] == 1) {
			return $img;
		}else return '';
	}
}