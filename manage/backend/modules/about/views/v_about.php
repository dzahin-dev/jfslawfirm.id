<?=mce5('mce_about',base_url('manage/news/upload_mce'));?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage About</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">

            <?=$this->load->view('include/top_banner',array('keyword'=>'about'));?>

            
            <div class="col-lg-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">About Content</h3>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">

                            <?=form_open_multipart('about/save',array("class"=>"form-horizontal"))?>

                            <div class="form-group row">
                                <label class="control-label col-md-2">Image</label>
                                <div class="col-md-10">
                                    <?php if (strlen($ab->image) > 0): ?>
                                        <a target="_blank" href="<?=base_url()?>media/images/<?=$ab->image?>">
                                            <img width="150" src="<?=base_url()?>media/images/<?=$ab->image?>">
                                        </a>
                                    <?php endif ?>
                                    <input type="file" class="default" name="userfile">
                                    <p class="text-muted">* Image size up to 1280 x 800 PX , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">Title</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="title" value="<?=$ab->title?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">Meta Keyword</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="meta_keyword" value="<?=$ab->meta_keyword?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">Meta Description</label>
                                <div class="col-md-5">
                                    <textarea name="meta_description" class="form-control"><?=$ab->meta_description?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 control-label">Content</label>
                                <div class="col-md-10">
                                    <textarea class="selector mce_about" style="margin-top: 30px;" name="content"><?=$ab->content?></textarea>  
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                            <?=form_close()?>                                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>