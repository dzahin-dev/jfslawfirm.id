<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title"><?=$tt?></h4>
            <div class="btn-group" style="float: right;">
                <a class="btn btn-sm btn-secondary <?=($act=='project')? 'active' : '' ;?> waves-effect" href="<?=base_url()?>manage/project">
                    Project List
                </a>
                <a class="btn btn-sm btn-secondary <?=($act=='category')? 'active' : '' ;?> waves-effect" href="<?=base_url()?>manage/project/category">
                    Project Category
                </a>
                <a class="btn btn-sm btn-secondary <?=($act=='banner')? 'active' : '' ;?> waves-effect" href="<?=base_url()?>manage/project/banner">
                    Project Banner
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>