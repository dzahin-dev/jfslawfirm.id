<?=mce5('mce_news',base_url('manage/news/upload_mce'));?>
<div class="content">
    <div class="container-fluid">

        <?$this->load->view('i_title',array('tt'=>'Manage Projects','act'=>'project'))?>

        <div class="row">
            <div class="col-lg-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Project Edit</h3>
                        <div class="btn-group">
                            <a class="btn btn-success waves-effect" href="<?=base_url()?>manage/project">
                                <i class="fa fa-arrow-left"></i>
                                project list
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Edit Images Project</h4>
                        <br>
                        <?=form_open_multipart('project/update_images/'.$project_id,array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="control-label col-md-2">image project</label>
                                <div class="col-md-10">
                                    <a target="_blank" href="<?=base_url()?>media/projects/<?=$project->image?>">
                                        <img width="100" src="<?=base_url()?>media/projects/<?=$project->image?>">
                                    </a>
                                    <input type="file" class="default" name="userfile">
                                    <p class="text-muted m-b-25">* Image size up to 800 x 600 PX , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                        <?=form_close()?>
                        <hr>
                        <br>
                        <h4 class="header-title m-t-0">Projects Content</h4><br>
                        <?=form_open_multipart('project/update_content/'.$project_id,array("class"=>"form-horizontal"))?>
                        <div class="form-group row">
                            <label class="control-label col-md-2">Meta Keyword</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="meta_keyword" value="<?=$project->meta_keyword?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-2">Meta Description</label>
                            <div class="col-md-7">
                                <textarea name="meta_description" class="form-control"><?=$project->meta_description?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-2">Category</label>
                            <div class="col-md-5">
                                <select name="category_id" class="form-control">
                                    <?php foreach ($cat->result() as $c): ?>
                                        <option value="<?=$c->id?>"><?=$c->title?></option>    
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-2">Title</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="title" value="<?=$project->title?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-2">Sub Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="subtitle" value="<?=$project->subtitle?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 control-label">Content</label>
                            <div class="col-md-10">
                                <textarea class="selector mce_news" style="margin-top: 30px;" placeholder="Type some text" name="content"><?=$project->content?></textarea>
                            </div>
                        </div>                                            
                        <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                        <?=form_close()?>                                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>