<div class="content">
    <div class="container-fluid">

        <?$this->load->view('i_title',array('tt'=>'Manage Projects','act'=>'project'))?>

        <div class="row">
            <div class="col-lg-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Products List</h3>
                        <div class="btn-group">
                            <a class="btn btn-success waves-effect" href="<?=base_url('manage/project/add')?>">
                                <i class="fa fa-plus"></i>
                                add project
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped mb-0">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Content</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=0; foreach ($projects->result() as $d): $i++;?>
                                    <tr>
                                        <th scope="row"><?=$i?></th>
                                        <td>
                                            <a target="_blank" href="<?=base_url()?>media/projects/<?=$d->image?>">
                                                <img style="width:100px;" src="<?=base_url()?>media/projects/<?=$d->image?>">
                                            </a>
                                        <td><?=$d->title?></td>
                                        <td><?=smart_trim($d->title,100)?></td>
                                        <td class="action">
                                            <a title="edit" href="<?=base_url()?>manage/project/edit/<?=$d->id?>" class="edit fa fa-pencil delete-list dz-tip"></a>
                                            <a title="delete" href="<?=base_url()?>manage/project/delete/<?=$d->id?>" class="delete fa fa-times-circle delete-list dz-tip confirm-delete"></a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>