<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "project";
	}
	function index(){
		$data['projects']		= $this->db->order_by('id','desc')->get('projects');
		$data['local_view'] 	= 'v_project_index';
		$this->load->view('v_manage',$data);
	}

	function add(){
		if (is_post()) {
			$data['image'] 		= $this->set_image()[0];
			$data['image_alt'] 	= $this->set_image()[1];
			$this->db->insert('projects',$data);

			$id   				= $this->db->insert_id();
			redirect(base_url()."manage/project/edit/$id");
		}

		$data['local_view'] 	= 'v_project_add';
		$this->load->view('v_manage',$data);
	}

	function edit($id){
		$data['cat'] 			= $this->db->get("project_categories");
		$data['project_id']		= $id;
		$data['project']		= $this->db->get_where('projects',array('id'=>$id))->row();
		$data['local_view'] 	= 'v_project_edit';
		$this->load->view('v_manage',$data);
	}

	function update_images($id){
		if (is_post()) {
			$img		= $this->set_image();
			
			$this->db->where('id', $id);
			$this->db->update('projects', array('image'=>$img[0],'image_alt'=>$img[1])); 
			
			$this->session->set_flashdata('message','Data saved successfully');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function update_content($project_id){
		$data = $this->input->post();

		$this->db->where('id', $project_id);
		$this->db->update('projects', $data);

		$this->session->set_flashdata('message','Data saved successfully');
		redirect(base_url()."manage/project/edit/$project_id");
	}

	function delete($id){
		$this->db->delete('projects', array('id' => $id)); 
		$this->session->set_flashdata('message','Data deleted successfully');
		redirect($_SERVER['HTTP_REFERER']);
	}

	function set_image(){
		$img = $this->global_util->upload_all('media/projects');
		if ($img[0] == 1) {
			return array($img[1]['file_name'],$img[1]['orig_name']);
		}else return '';
	}

	function banner(){
		$data['local_view'] 	= 'v_banner';
		$this->load->view('v_manage',$data);
	}
}