<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "product";
	}
	function index(){
		$data['c']			= $this->db->order_by('id','desc')->get('project_categories');
		$data['local_view'] = 'v_category';
		$this->load->view('v_manage',$data);		
	}

	function add(){
		if (is_post()) {
			$data = $this->input->post();	
			$this->db->insert('project_categories',$data);	
			redirect(base_url()."manage/project/category");
		}
	}

	function delete($id){
		$this->db->delete('project_categories', array('id' => $id)); 
		$this->session->set_flashdata('message','Data deleted successfully');
		redirect($_SERVER['HTTP_REFERER']);
	}
}