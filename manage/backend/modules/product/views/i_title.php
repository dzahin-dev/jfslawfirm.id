<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title"><?=$tt?></h4>
            <div class="btn-group" style="float: right;">
                <a class="btn btn-sm btn-secondary <?=($act=='product')? 'active' : '' ;?> waves-effect" href="<?=base_url()?>manage/product">
                    Product List
                </a>
                <a class="btn btn-sm btn-secondary <?=($act=='category')? 'active' : '' ;?> waves-effect" href="<?=base_url()?>manage/product/category">
                    Product Category
                </a>
                <a class="btn btn-sm btn-secondary <?=($act=='banner')? 'active' : '' ;?> waves-effect" href="<?=base_url()?>manage/product/banner">
                    Product Banner
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>