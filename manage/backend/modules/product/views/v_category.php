<div class="content">
    <div class="container-fluid">
        <?$this->load->view('i_title',array('tt'=>'Manage Products','act'=>'category'))?>
        <div class="row">
            <div class="col-lg-6">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Category List</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped mb-0">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Cattegory</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=0; foreach ($c->result() as $d): $i++;?>
                                    <tr>
                                        <th scope="row"><?=$i?></th>
                                        <td><?=$d->name?></td>
                                        <td class="action">
                                            <a title="delete" href="<?=base_url()?>manage/product/category/delete/<?=$d->id?>" class="delete fa fa-times-circle delete-list dz-tip confirm-delete"></a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Add Category</h3>
                    </div>
                    <div class="card-body">
                        <?=form_open_multipart('product/category/add',array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="control-label col-md-12">Category Name</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="name">
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>