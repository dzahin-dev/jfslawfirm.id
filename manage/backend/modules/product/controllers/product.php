<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "product";
	}
	function index(){
		$data['products']		= $this->db->order_by('id','desc')->get('products');
		$data['local_view'] 	= 'v_product_index';
		$this->load->view('v_manage',$data);
	}

	function add(){
		if (is_post()) {
			$data['image'] 		= $this->set_image()[0];
			$data['image_alt'] 	= $this->set_image()[1];
			$this->db->insert('products',$data);

			$id   				= $this->db->insert_id();
			redirect(base_url()."manage/product/edit/$id");
		}

		$data['local_view'] 	= 'v_product_add';
		$this->load->view('v_manage',$data);
	}

	function edit($id){
		$data['cat'] 			= $this->db->get("product_categories");
		$data['product_id']		= $id;
		$data['product']		= $this->db->get_where('products',array('id'=>$id))->row();
		$data['local_view'] 	= 'v_product_edit';
		$this->load->view('v_manage',$data);
	}

	function update_images($id){
		if (is_post()) {
			$img		= $this->set_image();
			
			$this->db->where('id', $id);
			$this->db->update('products', array('image'=>$img[0],'image_alt'=>$img[1])); 
			
			$this->session->set_flashdata('message','Data saved successfully');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function update_content($product_id){
		$data = $this->input->post();

		$this->db->where('id', $product_id);
		$this->db->update('products', $data);

		$this->session->set_flashdata('message','Data saved successfully');
		redirect(base_url()."manage/product/edit/$product_id");
	}

	function delete($id){
		$this->db->delete('products', array('id' => $id)); 
		$this->session->set_flashdata('message','Data deleted successfully');
		redirect($_SERVER['HTTP_REFERER']);
	}

	function set_image(){
		$img = $this->global_util->upload_all('media/products');
		if ($img[0] == 1) {
			return array($img[1]['file_name'],$img[1]['orig_name']);
		}else return '';
	}

	function banner(){
		$data['local_view'] 	= 'v_banner';
		$this->load->view('v_manage',$data);
	}
}