<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "product";
	}
	function index(){
		$data['c']			= $this->db->order_by('id','desc')->get('product_categories');
		$data['local_view'] = 'v_category';
		$this->load->view('v_manage',$data);		
	}

	function add(){
		if (is_post()) {
			$data = $this->input->post();	
			$this->db->insert('product_categories',$data);	
			redirect(base_url()."manage/product/category");
		}
	}

	function delete($id){
		$this->db->delete('product_categories', array('id' => $id)); 
		$this->session->set_flashdata('message','Data deleted successfully');
		redirect($_SERVER['HTTP_REFERER']);
	}
}