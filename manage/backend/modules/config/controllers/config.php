<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Config extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "config";
	}

	function index(){
		if (is_post()) {
			$data = $this->input->post();
			$this->db->where('id', 1);
			$this->db->update('global_texting', $data);

			$this->session->set_flashdata('message','Data Saved Successfully');	
			redirect(base_url('manage/config'));
		}	

		$data['c'] 				= $this->db->get('global_texting')->row_array();
		$data['tp'] 			= $this->db->field_data('global_texting');

		$data['local_view'] 	= 'v_config';
		$this->load->view('v_manage',$data);
	}
}