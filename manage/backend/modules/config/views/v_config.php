<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Site Configuration</h4>
                    <div class="clearfix">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title m-t-0">Site Configuration Management</h4>
                        <br>
                        <?=form_open()?>

                        <?php foreach ($tp as $t): ?>
                            
                            <?php if ($t->name != "id"): ?>
                                
                                <div class="form-group row">
                                    <label class="col-md-3 control-label"><?=$t->name;?></label>
                                    <div class="col-md-9">

                                        <?php if ($t->type == "varchar"): ?>
                                            
                                            <input type="text" name="<?=$t->name;?>" class="form-control" value="<?=$c[$t->name];?>">

                                        <?php else: ?>

                                            <textarea style="font-size:10px" class="form-control" name="<?=$t->name;?>"><?=$c[$t->name];?></textarea>

                                        <?php endif ?>

                                        
                                    </div>
                                </div>

                            <?php endif ?>


                        <?php endforeach ?>


                        <button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
                        <?=form_close()?>      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>