<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Footer extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "footer";
	}
	function index(){
		$lang = $this->db->limit(1)->get('master_language')->row();
		redirect(base_url().'manage/footer/show/'.$lang->id);
	}

	function show($lang=1){
		$data['footer_lang']	= $this->get_data_footer_lang();
		$data['master_lang']	= $this->db->get('master_language');
		$data['selected_lang']	= $lang;

		$data['local_view'] 	= 'v_footer';
		$this->load->view('v_manage',$data);
	}

	function update_content($id,$lang_id){
		$data = $this->input->post();
		$this->db->where('id', $id);
		$this->db->update('ml_footer', $data);

		$this->session->set_flashdata('message','Data saved successfully');
		redirect(base_url()."manage/footer/show/$lang_id");
	}

	function save_content($cat_id,$lang_id){
		$data = $this->input->post();
		$data['lang_id'] 		=  $lang_id;
		$this->db->insert('ml_footer',$data);
		$this->session->set_flashdata('message','Data saved successfully');
		redirect(base_url()."manage/footer/show/$lang_id");
	}

	function delete_content($id,$lang_id){
		$this->db->delete('ml_footer', array('id'=>$id));
		$this->session->set_flashdata('message','Data deleted successfully');
		redirect(base_url()."manage/show/$lang_id");
	}

	function get_data_footer_lang(){
		$lang = $this->db->get('ml_footer');
		$data = array();
		foreach ($lang->result() as $d) {
			$data[$d->lang_id] = $d;
		}
		return $data;
	}
}