<?=$this->load->view('include/froala_editor');?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Manage Footer</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Footer Edit</h3>
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-tabs tabs-lang" role="tablist">
                            <?php foreach ($master_lang->result() as $lang): ?>
                                <li class="nav-item">
                                    <a class="nav-link <?=($lang->id == $selected_lang)? 'active' : '';?>" id="lang-tab-<?=$lang->id?>" data-toggle="tab" href="#lang-<?=$lang->id?>" role="tab" aria-controls="lang-<?=$lang->id?>" aria-selected="<?=($lang->id == $selected_lang)? 'true' : 'false';?>">
                                        <img class="lang_flag" src="<?=base_url()?>assets/images/lang/<?=$lang->flag?>">
                                        <span class="d-none d-sm-block"><?=$lang->language?></span>
                                    </a>
                                </li>    
                            <?php endforeach ?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach ($master_lang->result() as $lang): ?>
                                <div class="tab-pane <?=($lang->id == $selected_lang)? 'active' : '';?>" id="lang-<?=$lang->id?>" role="tabpanel" aria-labelledby="lang-tab-<?=$lang->id?>">
                                    <?php if (isset($footer_lang[$lang->id])): ?>
                                        <?=form_open_multipart('footer/update_content/'.$footer_lang[$lang->id]->id."/".$lang->id,array("class"=>"form-horizontal"))?>
                                            <div class="form-group row">
                                                <label class="control-label col-md-2">title_1</label>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control" name="title_1" value="<?=$footer_lang[$lang->id]->title_1?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-2">title_2</label>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control" name="title_2" value="<?=$footer_lang[$lang->id]->title_2?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-2">address 1</label>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control" name="ad1" value="<?=$footer_lang[$lang->id]->ad1?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-2">address 2</label>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control" name="ad2" value="<?=$footer_lang[$lang->id]->ad2?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-2">address 3</label>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control" name="ad3" value="<?=$footer_lang[$lang->id]->ad3?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-2">hello</label>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control" name="hello" value="<?=$footer_lang[$lang->id]->hello?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-2">email</label>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control" name="email" value="<?=$footer_lang[$lang->id]->email?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-2">telp</label>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control" name="telp" value="<?=$footer_lang[$lang->id]->telp?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-2">sos_fb</label>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control" name="sos_fb" value="<?=$footer_lang[$lang->id]->sos_fb?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-2">sos_ig</label>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control" name="sos_ig" value="<?=$footer_lang[$lang->id]->sos_ig?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-2">sos_tw</label>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control" name="sos_tw" value="<?=$footer_lang[$lang->id]->sos_tw?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-2">sos_yt</label>
                                                <div class="col-md-5">
                                                    <input type="text" class="form-control" name="sos_yt" value="<?=$footer_lang[$lang->id]->sos_yt?>">
                                                </div>
                                            </div>
                                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                                            <a href="<?=base_url()?>manage/footer/delete_content/<?=$footer_lang[$lang->id]->id?>/<?=$lang->id?>" class="btn btn-danger waves-effect waves-light m-t-20 confirm-delete">Delete</a>
                                        <?=form_close()?>                                        
                                    <?php else: ?>
                                        <?=form_open_multipart('footer/save_content/'.$lang->id."/".$lang->id,array("class"=>"form-horizontal"))?>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">title_1</label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" name="title_1">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">title_2</label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" name="title_2">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">address 1</label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" name="ad1">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">address 2</label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" name="ad2">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">address 3</label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" name="ad3">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">hello</label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" name="hello">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">email</label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" name="email">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">telp</label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" name="telp">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">sos_fb</label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" name="sos_fb">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">sos_ig</label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" name="sos_ig">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">sos_tw</label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" name="sos_tw">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="control-label col-md-2">sos_yt</label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" name="sos_yt">
                                            </div>
                                        </div>
                                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                                        <?=form_close()?>
                                    <?php endif ?>
                                </div>
                            <?endforeach?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>