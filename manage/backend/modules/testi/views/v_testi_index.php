<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Testimonial</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Testimonial List</h3>
                        <div class="btn-group">
                            <a class="btn btn-success waves-effect" href="<?=base_url()?>manage/testi/add">
                                add testimonial
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped mb-0">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Testimonial</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=0; foreach ($testi->result() as $d): $i++;?>
                                    <tr>
                                        <th scope="row"><?=$i?></th>
                                        <td>
                                            <a target="_blank" href="<?=base_url()?>media/images/<?=$d->image?>">
                                                <img style="width:100px;" src="<?=base_url()?>media/images/<?=$d->image?>">
                                            </a>
                                        </td>
                                        <td><?=$d->title?></td>
                                        <td><?=smart_trim($d->description,100)?></td>
                                        <td class="action">
                                            <a title="edit" href="<?=base_url()?>manage/testi/edit/<?=$d->id?>" class="edit fa fa-pencil delete-list dz-tip"></a>
                                            <a title="delete" href="<?=base_url()?>manage/testi/delete/<?=$d->id?>" class="delete fa fa-times-circle delete-list dz-tip confirm-delete"></a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>