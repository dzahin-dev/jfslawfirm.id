<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Testimonial</h4>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card ml_card">
                    <div class="card-heading bg-inverse">
                        <h3 class="card-title text-white">Testimonial Edit</h3>
                        <div class="btn-group">
                            <a class="btn btn-success waves-effect" href="<?=base_url()?>manage/testi">
                                <i class="fa fa-arrow-left"></i>
                                Testimonial list
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <?=form_open_multipart('testi/update_images/'.$testi_id,array("class"=>"form-horizontal"))?>
                            <div class="form-group row">
                                <label class="control-label col-md-2">photo</label>
                                <div class="col-md-10">
                                    <a target="_blank" href="<?=base_url()?>media/images/<?=$testi->image?>">
                                        <img width="100" src="<?=base_url()?>media/images/<?=$testi->image?>">
                                    </a>
                                    <input type="file" class="default" name="userfile">
                                    <p class="text-muted m-b-25">* Image size up to 400 x 400 PX , JPG & PNG allowed.</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">Title</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="title" value="<?=$testi->title?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-2">Testimonial</label>
                                <div class="col-md-7">
                                    <textarea class="form-control" name="description"><?=$testi->description?></textarea>
                                </div>
                            </div>
                            <button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Save</button>
                        <?=form_close()?>
                        <hr>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
