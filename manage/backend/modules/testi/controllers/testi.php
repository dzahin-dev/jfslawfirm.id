<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testi extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "testi";
	}
	function index(){
		$data['testi']			= $this->db->order_by('id','desc')->get('testimonial');
		$data['local_view'] 	= 'v_testi_index';
		$this->load->view('v_manage',$data);		
	}

	function add(){
		if (is_post()) {
			$img				= $this->global_util->upload('media/images');
			$data 				= $this->input->post();
			$data['image'] 		= $img[1]['file_name'];
			$data['image_alt'] 	= $img[1]['orig_name'];

			$this->db->insert('testimonial',$data);	

			redirect(base_url()."manage/testi");
		}

		$data['local_view'] 	= 'v_testi_add';
		$this->load->view('v_manage',$data);
	}

	function edit($id){
		$data['testi_id']		= $id;
		$data['testi']			= $this->db->get_where('testimonial',array('id'=>$id))->row();
		$data['local_view'] 	= 'v_testi_edit';
		$this->load->view('v_manage',$data);
	}

	function update_images($id){
		if (is_post()) {
			$img		= $this->global_util->upload('media/images');
			
			$data 				= $this->input->post();
			
			if ($img[0]) {
				$data['image'] 		= $img[1]['file_name'];
				$data['image_alt'] 	= $img[1]['orig_name'];
			}

			$this->db->where('id', $id);
			$this->db->update('testimonial', $data); 
			
			$this->session->set_flashdata('message','Data saved successfully');
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function delete($id){
		$this->db->delete('testimonial', array('id' => $id)); 
		$this->session->set_flashdata('message','Data deleted successfully');
		redirect($_SERVER['HTTP_REFERER']);
	}

	function set_active(){
		$input = $this->input->post();
		$this->db->where('id', $input['id']);
		$this->db->update('testimonial', array('active'=>$input['status']));
	}
}