<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Global_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $c              = $this->db->get('global_texting')->row_array();
        $tp             = $this->db->field_data('global_texting');

        foreach ($tp as $k => $v) {
            if($v->name != 'id'){
                $this->{$v->name} = $c[$v->name];    
            }
        }
    }

    function get_master_lang(){
        return $this->db->get('master_language');
    }

    function get_home_product($ids){  
        $sql = "select * from products where id in ($ids)";
        return $this->db->query($sql);
    }

    function get_home_project(){
        $sql = "select pc.title as category,p.* from projects p join project_categories pc on p.category_id = pc.id order by id desc limit 8";
        $project['item']    = $this->db->query($sql);
        $project['cat']     = $this->db->get('project_categories');

        return $project;
    }

    function get_footer_menu(){
        return $this->db->order_by('menu_sort asc')->get('gs_menu_footer');
    }

    function get_banner($key){
        return $this->db->get_where('top_banner',array('keyword'=>$key))->row(); 
    }

}