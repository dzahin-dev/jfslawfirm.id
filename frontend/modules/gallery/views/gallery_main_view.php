<!doctype html>
<html class="no-js" lang="en">

<head>
    <?= $this->load->view('include/header_script') ?>
</head>

<body>

    <div id="preloader"></div>

    <div id="wrapper" class="wrapper">
        <?= $this->load->view('component/header') ?>

        <?= $this->load->view('component/banner', array('keyword' => 'gallery')) ?>

        <div class="section-space">
            <div class="container" id="isotope-container">
                <div class="row featuredContainer zoom-gallery">

                    <? foreach ($img->result() as $i):?>

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-mb-12 insurance employment">
                        <div class="project-box-layout1">
                            <img src="<?= base_url() ?>media/gallery/<?= $i->image ?>" class="img-responsive" alt="project">
                            <div class="d-flex align-items-center">
                                <div class="item-content">
                                    <a href="<?= base_url() ?>media/gallery/<?= $i->image ?>" class="elv-zoom" title="<?=$i->title?>">
                                        <i class="fa fa-search-plus" aria-hidden="true"></i>
                                    </a>
                                    <h3 class="title-medium-light size-sm"><?=$i->title?></h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?endforeach;?>
                </div>
            </div>
        </div>



        <?= $this->load->view('component/footer') ?>

    </div>

    <?= $this->load->view('include/footer_script') ?>

</body>

</html>