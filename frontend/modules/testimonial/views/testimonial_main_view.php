<!doctype html>
<html class="no-js" lang="en">
<head>
    <?=$this->load->view('include/header_script')?>
</head>
<body>

    <?=$this->load->view('component/top_nav')?>
    
        <section class="content-top-margin page-title page-title-small border-top-light">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12 wow fadeInUp" data-wow-duration="300ms">
                        <h1 class="black-text">Testimonials</h1>
                    </div>
                </div>
            </div>
        </section>

        <section id="slider" class="wow fadeIn fix-background" style="background-image:url('<?=base_url()?>assets/images/parallax-img43.jpg');">
            <div class="opacity-full bg-dark-gray"></div>
            <div class="container">
                <div class="row">
                    <div id="owl-demo" class="col-xs-12 owl-carousel owl-theme light-pagination bottom-pagination dark-pagination-without-next-prev-arrow position-relative">
                        
                        <?php foreach ($t->result() as $ts): ?>
                            
                            <div class="item">
                                <div class="col-md-5 col-sm-8 no-padding testimonial-style2 center-col text-center margin-three no-margin-top">
                                    <img src="<?=base_url()?>media/images/<?=$ts->image?>" alt="<?=$ts->image_alt?>"/>
                                    <p class="white-text text-med"><?=$ts->description?></p>
                                    <span class="name white-text text-med"><?=$ts->title?></span>
                                </div>
                            </div>

                        <?php endforeach ?>
                        
                    </div>
                </div>
            </div>
        </section>

    <?=$this->load->view('component/footer')?>

    <?=$this->load->view('include/footer_script')?>
    <style type="text/css">
        .navbar-default .navbar-nav > li > a {
            color: #000 !important;
        }
    </style>
</body>
</html>