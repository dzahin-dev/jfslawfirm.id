<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testimonial extends CI_Controller {	

	function __construct(){
		parent::__construct();
	}

	public function index(){
		$data['t'] = $this->db->order_by('id desc')->get('testimonial');

		$data['custom_title'] 		= "TESTIMONIAL";

		$this->load->view('testimonial_main_view',$data);
	}
}