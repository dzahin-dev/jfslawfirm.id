<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {	

	function __construct(){
		parent::__construct();
	}

	public function index(){
		$data['c'] = $this->db->get('contact')->row();

		$data['custom_title'] 		= "Contact Us";
		$data['custom_keyword'] 	= $data['c']->meta_keyword;
		$data['custom_description'] = $data['c']->meta_description;

		$this->load->view('contact_main_view',$data);
	}

	function save_form_contact(){
		$input = $this->input->post();
        $this->db->insert('contact_form',$input);
        echo "success";
	}
}