<!doctype html>
<html class="no-js" lang="en">

<head>
    <?= $this->load->view('include/header_script') ?>
</head>

<body>

    <div id="preloader"></div>

    <div id="wrapper" class="wrapper">
        <?= $this->load->view('component/header') ?>

        <?= $this->load->view('component/banner', array('keyword' => 'contact')) ?>

        <div id="wrapper" class="wrapper">

            <div class="map-layout1 fixed-menu-mt">
                <div class="container-fluid">
                    <div class="row">
                        <div class="google-map-area">
                            <?= $c->map ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="container">
                <div class="row no-gutters">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="service-box-layout3 bg-primary">
                            <i class="fa fa-file-text" aria-hidden="true"></i>
                            <h3 class="title-medium-light size-xl">Contact Information</h3>
                            <ul class="text-center">
                                <li><?=$c->office_1?></li>
                                <li><?=$c->address_1?></li>
                                <li><?=$c->telp_1?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="service-box-layout3 bg-dark-primary">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <h3 class="title-medium-light size-xl">Address</h3>
                            <ul class="text-center">
                                <li><?=$c->office_2?></li>
                                <li><?=$c->address_2?></li>
                                <li><?=$c->telp_2?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="service-box-layout3 bg-primary">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            <h3 class="title-medium-light size-xl">Schedule</h3>
                            <ul class="text-center">
                                <li><?=$c->office_3?></li>
                                <li><?=$c->address_3?></li>
                                <li><?=$c->telp_3?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            <div class="container section-space-contact">

                <?= form_open(base_url('contact/save_form_contact'), array('id' => 'contact-form', 'class' => 'contact-form', 'novalidate' => 'true')) ?>
                <fieldset>
                    <div class="row">
                        <div class="col-lg-offset-2 col-md-offset-2 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-sm-12">
                                    <div class="form-group">
                                        <input type="text" placeholder="Your Name" class="form-control" name="name" id="form-name" data-error="Name field is required" required="">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-sm-12">
                                    <div class="form-group">
                                        <input type="email" placeholder="Email Address" class="form-control" name="email" id="form-email" data-error="Email Address field is required" required="">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-sm-12">
                                    <div class="form-group">
                                        <input type="text" placeholder="Your Phone" class="form-control" name="phone" id="form-phone" data-error="Your Phone field is required" required="">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-sm-12">
                                    <div class="form-group">
                                        <input type="text" placeholder="Your Company" class="form-control" name="company" id="form-company" data-error="Your Company Name field is required" required="">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <textarea placeholder="Your case details" class="textarea form-control" name="comment" id="form-comment" rows="7" cols="20" data-error="Message field is required" required=""></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-sm-12">
                                    <div class="form-group text-center mb-none">
                                        <button type="submit" class="btn-fill-primary2 mt-30 disabled">Submit Request</button>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-sm-12">
                                    <div class="form-response"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <?= form_close(); ?>
            </div>




        </div>



        <?= $this->load->view('component/footer') ?>

    </div>


    <?= $this->load->view('include/footer_script') ?>

</body>

</html>