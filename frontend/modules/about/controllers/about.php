<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {	

	function __construct(){
		parent::__construct();
	}

	public function index(){
        $data['a'] 				= $this->db->get('about')->row();

		$data['custom_title'] 		= $data['a']->title;
		$data['custom_keyword'] 	= $data['a']->meta_keyword;
		$data['custom_description'] = $data['a']->meta_description;

		$data['pop'] 	= $this->db->order_by('id','RANDOM')->limit(5)->get('news');
		$this->load->view('about_main_view',$data);
	}
}