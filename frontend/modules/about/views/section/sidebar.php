<?$link = json_decode($a->links);?>

<div class="widget">
	<h5 class="widget-title font-alt">Popular Blog</h5>
	<div class="thin-separator-line bg-dark-gray no-margin-lr"></div>
	<div class="widget-body">
		<ul class="widget-posts">
			<?php foreach ($pop->result() as $p): ?>

				<li class="clearfix">
					<a href="<?=base_url()?>blog/detail/<?=$p->id?>/<?=url_title($p->title)?>">
						<img src="<?=base_url()?>media/news/<?=$p->image?>" alt="<?=$p->image_alt?>" alt="">
					</a>
					<div>
						<a href="<?=base_url()?>blog/detail/<?=$p->id?>/<?=url_title($p->title)?>"><?=$p->title?></a> 
						<?=pretty_date($p->created_date)?>
					</div>
				</li>

			<?php endforeach ?>
		</ul>
	</div>
</div>