<!doctype html>
<html class="no-js" lang="en">

<head>
    <?= $this->load->view('include/header_script') ?>
</head>

<body>
    <div id="preloader"></div>

    <div id="wrapper" class="wrapper">
        <?= $this->load->view('component/header') ?>

        <?= $this->load->view('component/banner', array('keyword' => 'about')) ?>

        <div class="section-space bg-body">
            <div class="container">
                <div class="section-title-dark">
                    <h2><span><?= $a->title ?></span> <?= $a->title ?></h2>
                    <span><i class="fa fa-circle-o" aria-hidden="true"></i></span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laborisnisi.</p>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <?= $a->content ?>
                    </div>
                </div>
            </div>
        </div>

        <?= $this->load->view('component/footer') ?>

    </div>

    <?= $this->load->view('include/footer_script') ?>
</body>

</html>