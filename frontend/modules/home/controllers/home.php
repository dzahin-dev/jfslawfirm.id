<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {	

	function __construct(){
		parent::__construct();
	}

	public function index(){
		$data['slider']		= $this->db->get_where('home_slider',array('active'=>1));
		$data['texting']	= $this->db->limit(1)->get('home_texting')->row();
		$data['pa']			= $this->db->get("home_pa");
		$data['pj'] 		= json_decode($data['texting']->proc_json,true);
		$data['news'] 		= $this->db->limit(3)->order_by('id','desc')->get('news');
		
		$this->load->view('home_main_view',$data);
	}

	function save_form_slider(){
		$input = $this->input->post();
		$this->db->insert('home_slider_form',$input);
	}
}