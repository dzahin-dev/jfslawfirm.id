<section id="section_calc" class="wow fadeIn bg-gray">
    <div class="container">
        <div class="row">
            <div id="container_calc" class="col-md-8 col-sm-10 center-col">
                <a class="close-calc" href="#">X</a>
                <form id="form-calculator">
                    <div class="title-calc" class="form-group">
                        <h2>DECON CALCULATOR</h2>
                        <B>HITUNG KEBUTUHAN DECON ANDA</b>
                    </div>
                    <div class="form-group">
                        <div class="select-style input-round big-input">
                            <select>
                                <option value="">Pilih Tipe Product</option>
                                <option value="">Interface Issue</option>
                                <option value="">Design</option>
                                <option value="">Production</option>
                                <option value="">Web development</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 col-sm-12"></div>
                            <div class="col-md-5 col-sm-12">
                                <label for="textbox" class="text-uppercase">Panjang Bidang</label>    
                            </div>
                            <div class="col-md-2 col-sm-8 col-xs-8">
                                <input type="text" id="textbox" name="name" class="input-round big-input" placeholder="1">
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-4">
                                <label for="textbox" class="text-uppercase meter">m</label>    
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 col-sm-12"></div>
                            <div class="col-md-5 col-sm-12">
                                <label for="textbox" class="text-uppercase">Lebar Bidang</label>    
                            </div>
                            <div class="col-md-2 col-sm-8 col-xs-8">
                                <input type="text" id="textbox" name="name" class="input-round big-input" placeholder="1">
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-4">
                                <label for="textbox" class="text-uppercase meter">m</label>    
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 col-sm-12"></div>
                            <div class="col-md-5 col-sm-12">
                                <label for="textbox" class="text-uppercase">Tinggi Bidang</label>    
                            </div>
                            <div class="col-md-2 col-sm-8 col-xs-8">
                                <input type="text" id="textbox" name="name" class="input-round big-input" placeholder="1">
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-4">
                                <label for="textbox" class="text-uppercase meter">m</label>    
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <br>
                        <button style="display: block;margin: 0 auto;" class="btn btn-black no-margin-bottom btn-medium btn-round no-margin-top" type="submit">Submit</button>
                        <br>
                    </div>
                    <div class="form-group">
                        <div class="row result">
                            <div class="col-md-6 col-s-6 col-xs-6">
                                <b>Volume Bidang</b>
                                <h3>20 M<span>3</span></h3>
                            </div>
                            <div class="col-md-6 col-s-6 col-xs-6">
                                <b>Kebutuhan Decon Anda</b>
                                <h3>860 Bag</h3>
                            </div>
                        </div>
                    </div>
                    <div id="calc-tweak">
                        <br><br><br><br>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
