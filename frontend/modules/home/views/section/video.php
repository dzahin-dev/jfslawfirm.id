<section class="fix-background" style="background-image:url('<?=base_url()?>assets/images/architecture-img4.jpg');">
    <div class="opacity-full bg-white"></div>
    <div class="container position-relative">
        <div class="row">
            <div class="col-md-6 col-sm-12 sm-margin-bottom-ten sm-text-center">
                <h3 class="title-med no-padding-bottom letter-spacing-2"><?=$texting->video_title?></h3>
                <p class="text-med margin-five"><?=$texting->video_description?></p>
            </div>
            <div class="col-md-5 col-md-offset-1 text-center margin-three xs-margin-bottom-ten" style="margin-top:0px !important">
                <img src="<?=base_url()?>media/images/<?=$texting->video_img?>" alt="<?=$texting->video_img_alt?>">
                <a id="vid-play" href="<?=$texting->video_url?>">
                    <i class="fa fa-play small-icon"></i>
                </a>
            </div>
        </div>
    </div>
</section>