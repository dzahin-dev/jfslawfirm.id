<? $prd = $this->global_model->get_home_product($texting->product_showcase)?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-8 dividers-header double-line center-col text-center margin-ten no-margin-top">
                <div class="subheader bg-white">
                    <h3 class="title-med no-padding-bottom letter-spacing-2"><?=$texting->product_title?></h3>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach ($prd->result() as $p): ?>
                <div class="col-md-3 col-sm-6 text-center sm-margin-bottom-ten">
                    <img alt="<?=$p->image_alt?>" src="<?=base_url()?>media/products/<?=$p->image?>">
                    <h5 class="margin-ten no-margin-bottom xs-margin-top-five"><?=$p->title?></h5>
                </div>      
            <?php endforeach ?>
        </div>
    </div>
</section>