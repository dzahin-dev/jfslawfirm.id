<section>
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-8 dividers-header double-line center-col text-center margin-ten no-margin-top">
                <div class="subheader bg-white">
                    <h3 class="title-med no-padding-bottom letter-spacing-2"><?=$texting->wedo_section_title?></h3>
                </div>
            </div>
        </div>
        <div class="row margin-five no-margin-top">
            <div class="col-md-4 col-sm-4 xs-margin-bottom-ten">
                <img alt="" src="<?=base_url()?>assets/images/<?=$texting->wedo_img1?>" alt="<?=$texting->wedo_img_alt1?>">
                <p class="text-uppercase letter-spacing-2 black-text font-weight-600 margin-ten no-margin-bottom"><?=$texting->wedo_title1?></p>
                <p class="margin-two text-med width-90"><?=$texting->wedo_description1?></p>
            </div>
            <div class="col-md-4 col-sm-4 xs-margin-bottom-ten">
                <img src="<?=base_url()?>assets/images/<?=$texting->wedo_img2?>" alt="<?=$texting->wedo_img_alt2?>">
                <p class="text-uppercase letter-spacing-2 black-text font-weight-600 margin-ten no-margin-bottom"><?=$texting->wedo_title2?></p>
                <p class="margin-two text-med width-90"><?=$texting->wedo_description2?></p>
            </div>
            <div class="col-md-4 col-sm-4">
                <img src="<?=base_url()?>assets/images/<?=$texting->wedo_img3?>" alt="<?=$texting->wedo_img_alt3?>">
                <p class="text-uppercase letter-spacing-2 black-text font-weight-600 margin-ten no-margin-bottom"><?=$texting->wedo_title3?></p>
                <p class="margin-two text-med width-90"><?=$texting->wedo_description3?></p>
            </div>
        </div>
    </div>
</section>