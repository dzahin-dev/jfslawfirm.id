<? $prj = $this->global_model->get_home_project()?>
<section id="projects" class="grid-wrap work-4col margin-top-section no-margin-top border-top no-padding-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-8 dividers-header double-line center-col text-center">
                <div class="subheader bg-white">
                    <h3 class="title-med no-padding-bottom letter-spacing-2"><?=$texting->project_title?></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row no-padding">
            <div class="col-lg-3 col-md-8 col-sm-11 margin-three center-col text-center">
                <h4 class="gray-text"><?=$texting->project_description?></h4>
            </div>
            <div class="col-md-12 text-center">
                <div class="text-center">
                    <ul class="portfolio-filter nav nav-tabs">
                        <li class="nav active"><a href="#" data-filter="*">Semua</a></li>
                        <?php foreach ($prj['cat']->result() as $p): ?>
                            <li class="nav">
                                <a href="#" data-filter=".cat_<?=$p->id?>"><?=$p->title?></a>
                            </li>    
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
            <div class="grid-gallery overflow-hidden">
                <div class="tab-content">
                     <ul class="masonry-items grid">
                        <?php foreach ($prj['item']->result() as $i): ?>
                            <li class="cat_<?=$i->category_id?>">
                                <figure>
                                    <div class="gallery-img">
                                        <a href="<?=base_url("project/detail/$i->id/".url_title($i->title))?>">
                                            <img src="<?=base_url()?>media/projects/<?=$i->image?>" alt="<?=$i->image_alt?>">
                                        </a>
                                    </div>
                                    <figcaption>
                                        <h3><a href="<?=base_url("project/detail/$i->id/".url_title($i->title))?>"><?=$i->title?></a></h3>
                                        <p><?=$i->category?></p>
                                    </figcaption>
                                </figure>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>