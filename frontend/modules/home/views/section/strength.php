<section class="fix-background" style="background-image:url('<?=base_url()?>assets/images/architecture-img4.jpg');">
    <div class="opacity-full bg-white"></div>
    <div class="container position-relative">
        <div class="row">
            <div class="col-md-4 col-sm-12 sm-margin-bottom-ten sm-text-center">
                <h3 class="title-med no-padding-bottom letter-spacing-2"><?=$texting->strength_title?></h3>
                <p class="text-med margin-five"><?=$texting->strength_description?></p>
                <a class="highlight-button-dark btn btn-small button" href="<?=$texting->strength_button_link?>"><?=$texting->strength_button_text?></a>
            </div>
            <div class="col-md-2 col-sm-4 col-md-offset-2 text-center margin-three xs-margin-bottom-ten">
                <i class="icon-profile-male medium-icon black-text display-block"></i>
                <h1 class="font-weight-600 margin-five no-margin-bottom"><?=$texting->strength_team?></h1>
                <p class="text-uppercase black-text letter-spacing-2 text-small margin-three">Team</p>
            </div>
            <div class="col-md-2 col-sm-4 text-center margin-three xs-margin-bottom-ten">
                <i class="icon-global medium-icon black-text display-block"></i>
                <h1 class="font-weight-600 margin-five no-margin-bottom"><?=$texting->strength_home?></h1>
                <p class="text-uppercase black-text letter-spacing-2 text-small margin-three">Perumahan Dibangun</p>
            </div>
            <div class="col-md-2 col-sm-4 text-center margin-three">
                <i class="icon-bargraph medium-icon black-text display-block"></i>
                <h1 class="font-weight-600 margin-five no-margin-bottom"><?=$texting->strength_office?></h1>
                <p class="text-uppercase black-text letter-spacing-2 text-small margin-three">Perkantoran Dibangun</p>
            </div>
        </div>
    </div>
</section>