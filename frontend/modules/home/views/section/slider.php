<div class="slider-area slider-layout-4 slider-overlay mt-243">
    <div class="bend niceties preview-1">
        <div id="ensign-nivoslider-3" class="slides">            
            <?php $i=0; foreach ($slider->result() as $s):$i++; ?>
                <img alt="<?=$s->image_alt?>" src="<?=base_url()?>media/slider/<?=$s->image?>" alt="slider" title="#slider-direction-<?=$i?>" />
            <?php endforeach ?>
        </div>

        <?php $i=0; foreach ($slider->result() as $s): $i++;?>
            <div id="slider-direction-<?=$i?>" class="t-cn slider-direction">
                <div class="slider-content s-tb slide-<?=$i?>">
                    <div class="title-container s-tb-c title-textPrimary">
                        <div class="mid-title mid-title-slide"><?=$s->text;?></div>
                        <?php if ($s->button_text != ""): ?>
                            <div class="slider-btn-area">
                                <a href="<?=$s->button_url;?>" class="btn-fill-primary"><?=$s->button_text;?></a>
                            </div>    
                        <?php endif ?>
                        
                    </div>
                </div>
            </div>    
        <?php endforeach ?>
        
    </div>
</div>