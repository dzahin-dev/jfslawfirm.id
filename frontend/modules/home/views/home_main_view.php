<html class="no-js" lang="">

<head>
    <?= $this->load->view('include/header_script') ?>
    <style type="text/css">
        .margin-20-fa{
            margin-top: 20px;
        }
    </style>
</head>

<body>
    <div id="preloader"></div>
    <div id="wrapper" class="wrapper">
        
       <?= $this->load->view('component/header') ?> 

        <?= $this->load->view('section/slider') ?>        

        <div class="search-layout1 bg-accent">
            <div class="container">
                <div class="row">
                    <div style="text-align: center;" class="col-lg-12 col-md-12   col-sm-12 col-xs-12">
                        <h2 class="title-bold-light size-xs mb-none"><?=$texting->jargon?></h2>
                    </div>
                </div>
            </div>
        </div>
        
        <section id="about" class="section-space-top50 bg-box">
            <div class="container">
                <div class="row">
                    <div class="text-center d-md-flex align-items-md-center">
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            <div class="about-padding">
                                <div class="section-title-inner-dark">
                                    <h2><span><?=$texting->about_title_1?></span> <?=$texting->about_title_2?></h2>
                                    <span><i class="fa fa-circle-o" aria-hidden="true"></i></span>
                                </div>
                                <p class="text-dark mb-40"><?=$texting->about_description?></p>
                                <?php if ($texting->about_button_text): ?>
                                    <a href="<?=$texting->about_button_url?>" class="btn-fill-primary2"><?=$texting->about_button_text?></a>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="img-d-inline-block text-center col-lg-6 col-md-12 col-sm-12">
                            <img src="<?=base_url()?>media/images/<?=$texting->about_image?>" class="img-responsive mt-20 m-auto" alt="about">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <div id="service" class="section-space bg-body">
            <div class="container">
                <div class="section-title-dark">
                    <h2><span><?=$texting->practice_title_1?></span> <?=$texting->practice_title_2?></h2>
                    <span><i class="fa fa-circle-o" aria-hidden="true"></i></span>
                    <p><?=$texting->practice_description?></p>
                </div>
            </div>
            <div class="container">
                <div class="row">

                    <?php foreach ($pa->result() as $p): ?>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="practice-box-layout3 bg-box item-mb">
                                <div class="item-img">
                                    <img src="<?=base_url()?>media/images/<?=$p->image?>" alt="pic practice1" class="img-responsive">
                                </div>
                                <div class="item-content">
                                    <h3 class="title-medium-dark size-sm"><?=$p->title?></h3>
                                    <p><?=$p->description?></p>
                                </div>
                            </div>
                        </div>                        
                    <?php endforeach ?>

                </div>
            </div>
        </div>
        <div class="process-content bg-common overlay-default text-center" style="background-image: url('<?=base_url()?>/media/images/<?=$texting->proc_image?>');">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2><?=$texting->process_title?></h2>
                        <p><?=$texting->process_description?></p>
                    </div>
                </div>
            </div>
        </div>
        <div style="padding: 0px" class="container-fluid">
            <div class="row no-gutters">
                <?php for ($i=1;$i<7;$i++): ?>

                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 col-mb-12">
                        <div class="process-list-box bg-accent <?=($i % 2 == 0) ? "secondary-bg-accent":""?>">
                            <span>0<?=$i?></span>
                            <h3><?=$pj['proc_json_title'][$i]?>n</h3>
                            <p><?=$pj['proc_json_description'][$i]?></p>
                        </div>
                    </div>

                <?php endfor ?>
            </div>
        </div>
        <!-- Process Layout End Here -->
        
        <!-- News Layout start Here -->
        <div id="news" class="section-space bg-box">
            <div class="container">
                <div class="section-title-dark">
                    <h2><span> <?=$texting->news_title_1?></span> <?=$texting->news_title_2?></h2>
                    <span><i class="fa fa-circle-o" aria-hidden="true"></i></span>
                    <p><?=$texting->news_description?></p>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <?php foreach ($news->result() as $b): ?>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="news-box-layout3 bg-body">
                                <div class="item-img">
                                    <img src="<?=base_url()?>media/news/<?=$b->image?>" alt="<?=$b->image_alt?>" class="img-responsive">
                                </div>
                                <div class="item-content">
                                    <h3 class="title-medium-dark size-sm">
                                        <a href="<?=base_url()?>news/detail/<?=$b->id?>/<?=url_title($b->title)?>">
                                            <?=$b->title?>
                                        </a>
                                    </h3>
                                    <p><?=smart_trim($b->content,100)?></p>
                                </div>
                            </div>
                        </div>    
                    <?php endforeach ?>
                </div>
                <div class="text-center mt-30">
                    <a href="<?=base_url()?>news" class="btn-fill-primary2">VIEW ALL NEWS</a>
                </div>
            </div>
        </div>
        
       <?= $this->load->view('component/footer') ?> 

    </div>
    <!--end wrapper -->
    
    <?= $this->load->view('include/footer_script') ?>
    
</body>
</html>