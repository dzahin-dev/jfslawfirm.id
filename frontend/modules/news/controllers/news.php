<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {	

	function __construct(){
		parent::__construct();
	}

	public function index(){
		$data['cat'] 	= $this->get_cat();
		$data['blog'] 	= $this->db->order_by('id desc')->get('news');
		$data['pop'] 	= $this->get_pop();

		$data['custom_title'] 		= 'BLOG';

		$this->load->view('blog_main_view',$data);
	}

	public function tag($t){
		$data['cat'] 	= $this->get_cat();
		$data['blog'] 	= $this->db->order_by('id desc')->like('categories', $t)->get('news');
		$data['pop'] 	= $this->get_pop();

		$data['custom_title'] 		= 'BLOG';

		$this->load->view('blog_main_view',$data);
	}

	function detail($id){
		$data['b'] 	= $this->db->get_where('news',array('id'=>$id))->row();
		$data['cat'] 	= $this->get_cat();
		$data['pop'] 	= $this->get_pop();

		$data['custom_title'] 		= $data['b']->title;
		$data['custom_keyword'] 	= $data['b']->meta_keyword;
		$data['custom_description'] = $data['b']->meta_description;

		$this->load->view('blog_detail_view',$data);
	}

	function get_pop(){
		$p = $this->db->order_by('id','RANDOM')
				->limit(5)
				->get('news');
		return $p;
	}

	function get_cat(){
		$cat = $this->db->query('select categories from news GROUP BY categories');
		$dd = array();

		foreach ($cat->result() as $k) {
			$d = explode(",", $k->categories);
			foreach ($d as $c) {
				$dd[$c] = 0;
			}
		}
		$result = array();
		foreach ($dd as $key => $value) {
			$result[] = $key;
		}
		return $result;
	}
}