<!doctype html>
<html class="no-js" lang="en">

<head>
    <?= $this->load->view('include/header_script') ?>
</head>

<body>

    <div id="preloader"></div>

    <div id="wrapper" class="wrapper">
        <?= $this->load->view('component/header') ?>

        <?= $this->load->view('component/banner', array('keyword' => 'blog')) ?>

        <div class="section-space bg-box">
            <div class="container">
                <div class="row">

                    <?php foreach ($blog->result() as $b) : ?>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-mb-12">
                            <div class="news-box-layout2 bg-body item-mb">
                                <div class="item-img">
                                    <img src="<?= base_url() ?>media/news/<?= $b->image ?>" alt="pic news7" class="img-responsive width-100">
                                </div>
                                <div class="item-content">
                                    <h3 class="title-medium-dark">
                                        <a href="<?= base_url() ?>news/detail/<?= $b->id ?>"><?= $b->title ?></a>
                                    </h3>
                                    <div class="news-date"><?= pretty_date($b->created_date) ?> | <?= $b->categories ?></div>
                                    <p><?= smart_trim($b->content, 100) ?></p>
                                    <a class="btn-ghost" href="<?= base_url() ?>news/detail/<?= $b->id ?>">Read More</a>
                                </div>
                            </div>
                        </div>

                    <?php endforeach ?>

                </div>



                <!-- <ul class="pagination-center">
                    <li><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                </ul> -->
            </div>
        </div>

        <?= $this->load->view('component/footer') ?>

    </div>


    <?= $this->load->view('include/footer_script') ?>

</body>

</html>