<!doctype html>
<html class="no-js" lang="en">

<head>
    <?= $this->load->view('include/header_script') ?>
</head>

<body>

    <div id="preloader"></div>

    <div id="wrapper" class="wrapper">
        <?= $this->load->view('component/header') ?>

        <?= $this->load->view('component/banner', array('keyword' => 'blog')) ?>



        <div class="bg-box section-space-all">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="box-shadow mb-30">
                            <img src="<?= base_url() ?>media/news/<?= $b->image ?>" alt="<?= $b->image_alt ?>" class="img-responsive" />
                            <div class="box-padding bg-body">
                                <h2 class="title-medium-dark size-xs"><?= $b->title ?></h2>
                                <ul class="blog-comments">
                                    <li><a href="#"><i class="fa fa-calendar-o" aria-hidden="true"></i><?= pretty_date($b->created_date) ?></a></li>
                                    <li><a href="#"><i class="fa fa-tag" aria-hidden="true"></i><?= $b->categories ?></a></li>
                                </ul>
                                <div class="mb-50">
                                    <?= $b->content ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="sidebar-wrapper">
                            <div class="sidebar-box bg-body">

                                <h3 class="title-medium-primary size-sm sidebar-box-padding border-bottom mb-none">News Category</h3>
                                <div class="box-padding">
                                    <ul class="practice-list">
                                        <?php foreach ($cat as $c) : ?>
                                            <li><a href="<?= base_url() ?>news/tag/<?= $c ?>"><?= $c ?></a></li>
                                        <?php endforeach ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <?= $this->load->view('component/footer') ?>

    </div>


    <?= $this->load->view('include/footer_script') ?>

</body>

</html>