<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {	

	function __construct(){
		parent::__construct();
	}

	public function index(){
		$data['prd'] 		= $this->db->order_by('id desc')->get('products');
		$data['cat'] 		= $this->db->get('product_categories');
		$data['cat_select'] = 0;

		$data['custom_title'] 		= "PRODUK";

		$this->load->view('products_main_view',$data);
	}

	public function category($id){
		$data['prd'] 	= $this->db->order_by('id desc')->get_where('products',array('category_id'=>$id));
		$data['cat'] 	= $this->db->get('product_categories');
		$data['cat_select'] = $id;

		$data['custom_title'] 		= "PRODUK";
		
		$this->load->view('products_main_view',$data);
	}

	public function detail($id){
		$data['prd'] 	= $this->db->order_by('id desc')->get_where('products',array('id'=>$id))->row();

		$data['custom_title'] 		= $data['prd']->title;
		$data['custom_keyword'] 	= $data['prd']->meta_keyword;
		$data['custom_description'] = $data['prd']->meta_description;

		$this->load->view('products_detail_view',$data);
	}
}