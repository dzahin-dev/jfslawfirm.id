<!doctype html>
<html class="no-js" lang="en">
<head>
    <?=$this->load->view('include/header_script')?>
</head>
<body>

    <?=$this->load->view('component/top_nav')?>
    
    <?=$this->load->view('component/banner',array('keyword'=>'products'))?>

    <section class="page-title page-title-small border-bottom-light border-top-light">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-7 breadcrumb text-uppercase wow fadeInUp xs-display-none animated" data-wow-duration="600ms" style="visibility: visible; animation-duration: 600ms; animation-name: fadeInUp;">
                    <ul>
                        <li><a href="<?=base_url()?>">Home</a></li>
                        <li><a href="<?=base_url()?>products">Products</a></li>
                        <li>Detail</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 zoom-gallery sm-margin-bottom-ten">
                    <a href="<?=base_url()?>media/products/<?=$prd->image?>">
                        <img src="<?=base_url()?>media/products/<?=$prd->image?>" alt="<?=base_url()?>media/products/<?=$prd->image_alt?>">
                    </a>
                </div>
                <div class="col-md-5 col-sm-12 col-md-offset-1">
                    <span class="product-name-details text-uppercase font-weight-600 letter-spacing-2 black-text"><?=$prd->title?></span>
                    <div class="separator-line bg-black no-margin-lr margin-five"></div>
                    <p><?=$prd->short_description?></p>
                    <div class="col-md-9 col-sm-9 no-padding margin-five">
                        <a class="highlight-button-dark btn btn-medium button" href="shop-cart.html">
                            <i class="icon-basket"></i>
                            Pesan Sekarang
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="border-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="tab-style1">
                        <div class="col-md-12 col-sm-12 no-padding">
                            <ul class="nav nav-tabs nav-tabs-light text-left">
                                <li class="active"><a href="#tab_sec1" data-toggle="tab">Deskripsi</a></li>
                                <li class=""><a href="#tab_sec2" data-toggle="tab">Area Pemakaian</a></li>
                                <li class=""><a href="#tab_sec3" data-toggle="tab">Data Teknis</a></li>
                                <li class=""><a href="#tab_sec4" data-toggle="tab">Cara Pemakaian</a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane med-text fade active in" id="tab_sec1">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <?=$prd->description?>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab_sec2">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <?=$prd->area?>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab_sec3">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <?=$prd->technical?>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab_sec4">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <?=$prd->usage?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?=$this->load->view('component/footer')?>

    <?=$this->load->view('include/footer_script')?>
</body>
</html>