<!doctype html>
<html class="no-js" lang="en">
<head>
    <?=$this->load->view('include/header_script')?>
</head>
<body>

    <?=$this->load->view('component/top_nav')?>
    
    <?=$this->load->view('component/banner',array('keyword'=>'products'))?>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-4 sidebar">                        
                    <div class="widget">
                        <h5 class="widget-title font-alt">Categories</h5>
                        <div class="thin-separator-line bg-dark-gray no-margin-lr margin-ten"></div>
                        <div class="widget-body">
                            <ul class="category-list">
                                <li><a class="<?=($cat_select == 0 ? 'active' : '')?>" href="<?=base_url()?>products/">All Product</a></li>
                                <?php foreach ($cat->result() as $c): ?>
                                    <li><a class="<?=($cat_select == $c->id ? 'active' : '')?>" href="<?=base_url()?>products/category/<?=$c->id?>/<?=url_title($c->name)?>"><?=$c->name?></a></li>    
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                </div>                    
                <div class="col-md-9 col-sm-8 col-md-offset-1">
                    <div class="product-listing margin-three">

                        <?php foreach ($prd->result() as $p): ?>
                            
                            <div class="col-md-6 col-sm-6">
                                <div class="home-product text-center position-relative overflow-hidden margin-ten no-margin-top">
                                    <a href="<?=base_url()?>products/detail/<?=$p->id?>/<?=url_title($p->title)?>">
                                        <img src="<?=base_url()?>media/products/<?=$p->image?>" alt="<?=$p->image_alt?>">
                                    </a>
                                    <span class="product-name text-uppercase">
                                        <a href="<?=base_url()?>products/detail/<?=$p->id?>/<?=url_title($p->title)?>"><?=$p->title?></a>
                                    </span>
                                    <span class="price black-text"><?=$p->sub_title?></span>
                                </div>
                            </div>

                        <?php endforeach ?>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <?=$this->load->view('component/footer')?>

    <?=$this->load->view('include/footer_script')?>
</body>
</html>