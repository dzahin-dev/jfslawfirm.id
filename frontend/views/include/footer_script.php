<script src="<?=base_url()?>assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/plugins.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/wow.min.js"></script>
<script src="<?=base_url()?>assets/vendor/slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendor/slider/home.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/vendor/OwlCarousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.meanmenu.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.scrollUp.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.counterup.min.js"></script>
<script src="<?=base_url()?>assets/js/waypoints.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0uuKeEkPfAo7EUINYPQs3bzXn7AabgJI"></script>
<script src="<?=base_url()?>assets/js/validator.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.nav.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.countdown.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/isotope.pkgd.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/select2.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/main.js" type="text/javascript"></script>