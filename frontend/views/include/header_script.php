<?
    $gt = $this->global_model;
    $site_title         = (isset($custom_title))? $custom_title.' | '.$gt->site_title : $gt->site_title;
    $meta_keyword       = (isset($custom_keyword))? $custom_keyword : $gt->site_meta_keyword;
    $meta_description   = (isset($custom_description))? $custom_description : $gt->site_meta_description;
?>

<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title><?= $site_title ?></title>

<meta name="description" content="<?= $meta_description ?>">
<meta name="keywords" content="<?= $meta_keyword ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="shortcut icon" href="<?= base_url() ?>assets/images/favicon.png">

<link rel="stylesheet" href="<?= base_url() ?>assets/css/normalize.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/main.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/animate.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/font-awesome.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/font/flaticon.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/OwlCarousel/owl.carousel.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/OwlCarousel/owl.theme.default.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/meanmenu.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/slider/css/nivo-slider.css" type="text/css" />
<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/slider/css/preview.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?= base_url() ?>assets/css/jquery.datetimepicker.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/magnific-popup.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/hover-min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
<script src="<?= base_url() ?>assets/js/modernizr-2.8.3.min.js"></script>

<!--[if IE]>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/style-ie.css" />
<![endif]-->
<!--[if IE]>
    <script src="js/html5shiv.js"></script>
<![endif]-->


<style>
    .inner-page-banner-area {
        padding: 225px 0 130px;
    }

    .section-space-all {
        padding: 80px 0;
    }
    #map_canvas1{
        width: 100%;
        height: 500px;
    }
</style>