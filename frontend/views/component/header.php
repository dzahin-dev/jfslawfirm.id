<? $gt = $this->global_model;?>
<header class="header" id="home">
	<div id="header-three" class="header-style-two header-fixed">
		<div class="header-top-bar">
			<div class="header-top-bar-top bg-primary">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-md-12 col-sm-12 hidden--xs">
							<div class="header-contact-info-two">
								<ul>
									<li><i class="fa fa-clock-o" aria-hidden="true"></i><?=$gt->header_open_title?><span> <?=$gt->header_open_text?></span></li>
									<li><i class="fa fa-location-arrow" aria-hidden="true"></i><?=$gt->header_address?></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-4 hidden--md">
							<div class="header-social">
								<ul>

									<?php if ($gt->social_fb != ""): ?>
										<li><a href="<?=$gt->social_fb?>"><i class="fa margin-20-fa fa-facebook" aria-hidden="true"></i></a></li>	
									<?php endif ?>

									<?php if ($gt->social_tw != ""): ?>
										<li><a href="<?=$gt->social_tw?>"><i class="fa margin-20-fa fa-twitter" aria-hidden="true"></i></a></li>	
									<?php endif ?>

									<?php if ($gt->social_ig != ""): ?>
										<li><a href="<?=$gt->social_ig?>"><i class="fa margin-20-fa fa-instagram" aria-hidden="true"></i></a></li>	
									<?php endif ?>

									<?php if ($gt->social_yt != ""): ?>
										<li><a href="<?=$gt->social_yt?>"><i class="fa margin-20-fa fa-youtube" aria-hidden="true"></i></a></li>	
									<?php endif ?>

								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="header-top-bar-bottom bg-body">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 col-md-4">
							<div class="media call-area">
								<i class="fa fa-phone pull-left" aria-hidden="true"></i>
								<div class="media-body">
									<p><?=$gt->header_phone_title?></p>
									<div class="call-number"><?=$gt->header_phone_text?></div>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<div class="logo-area">
								<a href="<?=base_url()?>"><img src="<?=base_url()?>assets/img/logo.png" alt="logo" class="img-responsive"></a>
							</div>
						</div>
						<div class="col-lg-4 col-md-4">
							<!-- <div class="header-search-two">
								<form id="top-search-form2">
									<input type="text" class="search-input" placeholder="Search...." required="">
									<a href="#" class="search-button"><i class="fa fa-search" aria-hidden="true"></i></a>
								</form>
							</div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="main-menu-area bg-body" id="sticker">
			<div class="container">
				<div class="row d-md-flex align-items-md-center">
					<div class="col-lg-12 col-md-12">
						<nav id="dropdown">
							<ul id="navOnePage">
								<li><a href="<?=base_url()?>">Home</a></li>
								<li><a href="<?=base_url()?>#about">About</a></li>
								<li><a href="<?=base_url()?>#service">Service</a></li>
								<li><a href="<?=base_url()?>gallery">Gallery</a></li>
								<li><a href="<?=base_url()?>news">News</a></li>
								<li><a href="<?=base_url()?>contact">Contact</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>