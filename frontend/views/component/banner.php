<? $bnr = $this->global_model->get_banner($keyword);?>
<section class="inner-page-banner-area mt-150" style="background-image: url('<?=base_url()?>/media/banner/<?=$bnr->image?>');">
    <div class="container">
        <div class="breadcrumbs-area">
            <h1><?=$bnr->title?></h1>
            <ul>
                <li><a href="#">Home</a> //</li>
                <li><?=$bnr->title?></li>
            </ul>
        </div>
    </div>
</section>