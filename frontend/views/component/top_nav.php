<nav role="navigation" class="navbar navbar-default navbar-fixed-top nav-transparent overlay-nav sticky-nav nav-border-bottom bg-transparent">
    <div class="container">
        <div class="row">
            <div class="col-md-2 pull-left">
                <a class="logo-light" href="<?=base_url()?>">
                    <img alt="" class="logo" src="<?=base_url()?>assets/images/logo.png">
                </a>
                <a class="logo-dark" href="<?=base_url()?>">
                    <img alt="" class="logo" src="<?=base_url()?>assets/images/logo-light.png">
                </a>
            </div>

            <div class="navbar-header col-sm-8 col-xs-2 pull-right">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> 
                    <span class="sr-only">Toggle navigation</span> 
                    <span class="icon-bar"></span> 
                    <span class="icon-bar"></span> 
                    <span class="icon-bar"></span> 
                </button>
            </div>
            <div class="col-md-8 no-padding-right accordion-menu text-right">
                <div class="navbar-collapse collapse">
                    <?=$this->menu->get()?>              
                </div>
            </div>
        </div>
    </div>
</nav>